//
//  gdevepilog.h
//  
//
//  Created by Allen Blaylock on 6/28/13.
//
//

#ifndef _gdevepilog_h
#define _gdevepilog_h

#include "epImageClass.h"
#include "png_.h"

/* Job types */
#define EPG_JOB_MONO 0
#define EPG_JOB_COLOR 1
#define EPG_JOB_3D 2

/* internal line buffer */
#define EPILOG_LINESIZE 512

typedef struct gx_device_epilog_s {
    /* superclass state */
    gx_device_vector_common;
    /* local state */
    int header;		/* whether we've written the file header */
    int dirty;		/* whether we need to rewrite the <g> element */
    int mark;		/* <g> nesting level */
    int page_count;	/* how many output_page calls we've seen */
    gx_color_index strokecolor, fillcolor;
    double linewidth;
    gs_line_cap startcap;
    gs_line_join linejoin;
    double miterlimit;
    gs_id lastID;
    ep_image* rasterLayers;
    unsigned char tabLevel;
    unsigned char pathLevel;
    char tabs[256];
    gs_const_string streamFName;
    epg_params params;
    FILE *fp;
    char fileName[EPILOG_LINESIZE];
    png_infop png_info;
    png_structp png_struct;
    png_color *palettep;
    png_byte **row_pointers;
    gs_fixed_point offsetPoint;
    int padding;
    int numImages;
    int numPaths;
    bool colorOK;
    FILE *fpcsv;
    bool csvOpen;
}gx_device_epilog;

#endif
