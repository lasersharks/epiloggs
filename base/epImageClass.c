//
//  epImageClass.c
//  gs
//
//  Created by Epilog on 6/3/13.
//
//

#include "string_.h"
#include "gx.h"
#include "gserrors.h"
#include "gdevvec.h"
#include "stream.h"
#include "epImageClass.h"
#include "gxpath.h"
#include "gxcpath.h"
#include "std.h"
#include "stdlib.h"
#include "gdevepilog.h"

#define HEX_OUTPUT_PATH ""


ep_image* make_ep_image(gx_device_vector * const dev,gs_id id)
{
    ep_image* nImage;
    gx_device_epilog* epilog = (gx_device_epilog*)dev;
    nImage = (ep_image*) malloc(sizeof(ep_image));
    if(nImage == 0)
    {
        return 0;
    }
    nImage->dirty = 0;
    nImage->left = 0;
    nImage->right = 0;
    nImage->image = make2dArray(2, dev->width);
    if_debug2m('|', dev->memory, "Allocated array for id: %ld with size 2x%d\n",(ulong)id,dev->width);
//    nImage->image = make2dArray(dev->MediaSize[0], dev->MediaSize[1]);
    if (nImage->image == 0) {
        //Failed to alloc image deconstruct	
        free(nImage);
        return 0;
    }
    nImage->width = dev->width;
    nImage->height = dev->height;
    nImage->id = id;
    nImage->thisRow = 0;
    nImage->activeRow = 0;
    nImage->fileCreated = 0;
    nImage->dumped = 0;
    nImage->fileName = 0;
    nImage->params = epilog->params;
    return nImage;
}

ep_image* add_ep_image(ep_image* img,gx_device_vector * const dev,gs_id id)
{
    if (id == img->id) {
        return img;
    }
    if(id > img->id)
    {
        if (img->right != 0) {
            img->right = add_ep_image(img->right,dev,id);
        }
        else
        {
            img->right = make_ep_image(dev,id);
        }
        if (img->right == 0) {
            //A bad malloc error has occured, what do we do?
        }
        if ((height_ep_image(img->right) - height_ep_image(img->left)) == 2) {
            img = rotateRight_ep_image(img);
        }
    }
    else
    {
        if (img->left != 0) {
            img->left = add_ep_image(img->left,dev,id);
        }
        else
        {
            img->left = make_ep_image(dev,id);
        }
        if (img->left == 0) {
            //A bad malloc error has occured, what do we do?
        }

        if ((height_ep_image(img->left) - height_ep_image(img->right)) == 2) {
            img = rotateLeft_ep_image(img);
        }
    }
    return img;
}

ep_image* search_ep_image(ep_image* const img, gs_id id)
{
    if (img == 0) {
        return 0 ;
    }
    if (img->id == id) {
        return img;
    }
    else if(img->id < id)
    {
        if (img->right != 0) {
            return search_ep_image(img->right,id);
        }
    }
    else
    {
        if (img->left != 0) {
            return search_ep_image(img->left              ,id);
        }
    }
    return 0;
}

ep_image* rotateLeft_ep_image(ep_image* img)
{
    ep_image* temp;
    temp = img->left;
    img->left = img->right;
    temp->right = img;
    img = temp;
    return img;
}

ep_image* rotateRight_ep_image(ep_image* img)
{
    ep_image* temp;
    temp = img->right;
    img->right = img->left;
    temp->left = img;
    img = temp;
    return img;
}

int** make2dArray(int x,int y)
{
    int** arr;
    int i,j;
    arr = (int**) malloc(x*sizeof(int*));
    if (arr == 0) {
        return 0;
    }
    for (i = 0; i < x; i++) {
        arr[i] = (int *) malloc(y*sizeof(int));
        if (arr[i] == 0) {
            //Malloc failed free all memory we have alloc'ed
            for(j = 0;j < i; j++)
            {
                free(arr[i]);
            }
            free(arr);
            return 0;
        }
        //Set the memory to be "black 100% alpha"
        //We made this up
        memset(arr[i], 0x00, y*sizeof(int));
//        for(j = 0;j < y;j++)
//        {
//            //Set the alpha channle to 0xff or fully opaque
//            memset(((char*)&arr[i][j])+3,0xff,sizeof(char));
//        }
    }
    return arr;
}

void free2dArray(int** arr,int x,int y)
{
    int i;
    if (arr == 0) {
        return;
    }
    for (i = 0; i < x; i++) {
        free(arr[i]);
    }
    free(arr);
    return;
}

void delete_ep_image(ep_image* img)
{
    if (img == 0) {
        return;
    }
    delete_ep_image(img->left);
    delete_ep_image(img->right);
    free2dArray(img->image, 2, img->width);
    free(img);
    return;
}

unsigned height_ep_image(ep_image* const img)
{
    unsigned left = 0;
    unsigned right = 0;
    if (img != 0) {
        if (img->left != 0) {
            left = height_ep_image(img->left);
        }
        if (img->right != 0) {
            right = height_ep_image(img->right);
        }
        return 1 + (left > right ? left : right);
    }
    return 0;
}

void dump_ep_image(ep_image* const img)
{
    char full_file_name[50];
    FILE *fid;
    int i,id;
    if (img == 0) {
        return;
    }
    sprintf(full_file_name,"%02ld_%s_%dx%dx%d.hex",(ulong)img->id,"test",img->width,img->height,4);
    fid = fopen(full_file_name,"wb");
    id = img->id;
    //This is a raster pixle
    fwrite(&id, sizeof(int), 1, fid);
    fwrite(&(img->width), sizeof(int), 1, fid);
    fwrite(&(img->height), sizeof(int), 1, fid);
    for (i = 0; i < img->width; i++) {
        fwrite(img->image[i], sizeof(int), img->width, fid);
    }
    fclose(fid);
}

void dump_ep_image_r(ep_image* const img)
{
    if (img != 0) {
        dump_ep_image(img);
    }
    else
    {
        return;
    }
    if (img->right) {
       dump_ep_image_r(img->right); 
    }
    if (img->left) {
        dump_ep_image_r(img->left);
    }
}

//We should also do the dithering here eventually.
char* dump_row_ep_image(ep_image* const img)
{
    //First calculate how long the row is
    int start,finish,length;
    char *full_file_name;
    FILE *fid;
    
    if (img == 0) {
        //The image was null or we could not allocate memory
        return 0;
    }
    start = 0;
    finish = img->width-1;
    //Find the start end end of the row that is not transparent
    while ((img->image[img->activeRow][start] & 0xff000000) == 0x0 && start < img->width) {
        start++;
    }
    while ((img->image[img->activeRow][finish] & 0xff000000) == 0x0 && finish > 0) {
        finish--;
    }
    length = finish-start+1;
    if (length <= 0) {
        //Either the row was all white or there was an error
        return 0;
    }
    //Open the file
    if (!img->fileCreated) {
        //This is the first run, overwrite the file
        img->fileName = malloc(256);
        if (!img->fileName) {
            return 0;
        }
        sprintf(img->fileName,"%s%06ld_raster.hex",HEX_OUTPUT_PATH,(ulong)img->id);
        full_file_name = img->fileName;
        fid = fopen(full_file_name, "w");
        
    }
    else
    {
        //Append to the end of the file
        full_file_name = img->fileName;
        fid = fopen(full_file_name,"ab");
    }
    if (fid == 0) {
        //We could not open the file.
        return 0;
    }
    
    if(!img->fileCreated)
    {
        //We need to write the image width and height for the first go
        fwrite(&(img->width), sizeof(int), 1, fid);
        fwrite(&(img->height), sizeof(int), 1, fid);
        img->fileCreated = 1;
        fflush(fid);
    }
    //Save the info to the file in the form x,y,length,data
    fwrite(&(start), sizeof(int), 1, fid);
    fwrite(&(img->thisRow), sizeof(int), 1, fid);
    fwrite(&(length), sizeof(int), 1, fid);
    switch (img->params.jobType) {
        case EPG_JOB_COLOR:
            fwrite(&(img->image[img->activeRow][start]), sizeof(int), length, fid);
            break;
        case EPG_JOB_MONO:
            //break;
        case EPG_JOB_3D:
            //break;
            
        default:
            fwrite(&(img->image[img->activeRow][start]), sizeof(int), length, fid);
            break;
    }
    
    // Close the file
    fclose(fid);
    return full_file_name;
    
}


