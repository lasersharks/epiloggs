//
//  svgImageClass.h
//  gs
//
//  Created by Epilog on 6/3/13.
//
//

#ifndef gs_svgImageClass_h
#define gs_svgImageClass_h

#include "gx.h"

typedef struct _svg_image svg_image;

typedef struct {
    int ditheringType;
    int jobType;
}svg_params;


struct _svg_image{
    struct _svg_image *left;
    struct _svg_image *right;
    int **image;
    unsigned char dirty;
    int width,height;
    int thisRow;
    unsigned char activeRow;
    gs_id id;
    unsigned char fileCreated;
    char *fileName;
    unsigned char dumped;
    svg_params params;
};

svg_image* make_svg_image(gx_device_vector * const dev,gs_id id);
svg_image* add_svg_image(svg_image* img,gx_device_vector * const dev,gs_id id);
svg_image* search_svg_image(svg_image* const img, gs_id id);
svg_image* rotateLeft_svg_image(svg_image* img);
svg_image* rotateRight_svg_image(svg_image* img);
int** make2dArraySvg(int x,int y);
void free2dArraySvg(int** arr,int x,int y);
void delete_svg_image(svg_image* img);
unsigned height_svg_image(svg_image* const img);
void dump_svg_image(svg_image* const img);
void dump_svg_image_r(svg_image* const img);
char* dump_row_svg_image(svg_image* const img);

#endif
