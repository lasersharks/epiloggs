//
//  epImageClass.h
//  gs
//
//  Created by Epilog on 6/3/13.
//
//

#ifndef gs_epImageClass_h
#define gs_epImageClass_h

#include "gx.h"

typedef struct _ep_image ep_image;

typedef struct {
    int ditheringType;
    int jobType;
}epg_params;


struct _ep_image{
    struct _ep_image *left;
    struct _ep_image *right;
    int **image;
    unsigned char dirty;
    int width,height;
    int thisRow;
    unsigned char activeRow;
    gs_id id;
    unsigned char fileCreated;
    char *fileName;
    unsigned char dumped;
    epg_params params;
};

ep_image* make_ep_image(gx_device_vector * const dev,gs_id id);
ep_image* add_ep_image(ep_image* img,gx_device_vector * const dev,gs_id id);
ep_image* search_ep_image(ep_image* const img, gs_id id);
ep_image* rotateLeft_ep_image(ep_image* img);
ep_image* rotateRight_ep_image(ep_image* img);
int** make2dArray(int x,int y);
void free2dArray(int** arr,int x,int y);
void delete_ep_image(ep_image* img);
unsigned height_ep_image(ep_image* const img);
void dump_ep_image(ep_image* const img);
void dump_ep_image_r(ep_image* const img);
char* dump_row_ep_image(ep_image* const img);

#endif
