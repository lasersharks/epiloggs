//
//  base64.h
//  EpilogGS
//
//  Created by Allen Blaylock on 6/28/13.
//  Copyright (c) 2013 Epilog. All rights reserved.
//

#ifndef __EpilogGS__base64__
#define __EpilogGS__base64__

#include "gx.h"


char *base64_encode(const unsigned char *data,
                    size_t input_length,
                    size_t *output_length);
unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length);
void base64_cleanup(void);


#endif /* defined(__EpilogGS__base64__) */
