 /* Copyright (C) 2013 Epilog Laser, Inc.
   All Rights Reserved.

   This software is provided AS-IS with no warranty, either express or
   implied.

   This software is distributed under license and may not be copied,
   modified or distributed except as expressly authorized under the terms
   of the license contained in the file LICENSE in this distribution.

   
*/

#include "string_.h"
#include "gx.h"
#include "gserrors.h"
#include "gdevvec.h"
#include "stream.h"
#include "epImageClass.h"
#include "gxcpath.h"
#include "std.h"
#include "gxtmap.h"		/* for gxdht.h */
#include "gxdht.h"
#include "gxfont.h"
#include "gxiparam.h"
#include "gxistate.h"
#include "gxpaint.h"
#include "gzpath.h"
#include "gzcpath.h"
#include "gdevepilog.h"
#include "base64.h";
#include "gxfont.h"
#include "gscspace.h"
#include "gsptype2.h"
#include "gxpcolor.h"
#include "gxdevice.h"
#include "gxfixed.h"
#include "gspath2.h"
#include "stdlib.h"
#include "gstext.h"

extern_st(st_gs_text_enum);
extern_st(st_gx_image_enum_common);

#define MAX_EPILOG_TABLE 10
#define PNG_NO_CONSOLE_IO
#define XML_DECL    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"

/* default resolution. */
#ifndef X_DPI
#  define X_DPI 100
#endif
#ifndef Y_DPI
#  define Y_DPI 100
#endif

#define PNG_PADDING 2

#define PRINT_FUNCTION 0

#define EPG_OPTION_DITHERINGTYPE	"DitheringType"
#define EPG_OPTION_JOBTYPE	"JobType"

/* Dithering types */
#define EPG_DITHER_STD 0
#define EPG_DITHER_BRIGHTEN 1
#define EPG_DITHER_LOWRES 2
#define EPG_DITHER_FLOYD 3
#define EPG_DITHER_JARVIS 4
#define EPG_DITHER_STUCKI 5

/* default constants */
#define EPILOG_DEFAULT_LINEWIDTH	1.0
#define EPILOG_DEFAULT_LINECAP	gs_cap_butt
#define EPILOG_DEFAULT_LINEJOIN	gs_join_miter
#define EPILOG_DEFAULT_MITERLIMIT	4.0

/* ---------------- Device definition ---------------- */



typedef struct {
    const char* p_name;
    int p_value;
} stringParamDescription;

#define epilog_device_body(dname, depth)\
  std_device_dci_type_body(gx_device_epilog, 0, dname, &st_device_epilog, \
                           MAX_EPILOG_TABLE * X_DPI, \
                           MAX_EPILOG_TABLE * Y_DPI, \
                           X_DPI, Y_DPI, \
                           (depth > 8 ? 3 : 1), depth, \
                           (depth > 1 ? 255 : 1), (depth > 8 ? 255 : 0), \
                           (depth > 1 ? 256 : 2), (depth > 8 ? 256 : 1))

static dev_proc_open_device(epilog_open_device);
static dev_proc_output_page(epilog_output_page);
static dev_proc_close_device(epilog_close_device);

static dev_proc_get_params(epilog_get_params);
static dev_proc_put_params(epilog_put_params);

static dev_proc_begin_typed_image(epilog_begin_typed_image);

static dev_proc_fill_path(epilog_fill_path);

static dev_proc_stroke_path(epilog_stroke_path);

static dev_proc_text_begin(epilog_text_begin);

static dev_proc_copy_mono(epilog_copy_mono);

#define epilog_device_procs \
{ \
        epilog_open_device, \
        NULL,                   /* get_initial_matrix */\
        NULL,                   /* sync_output */\
        epilog_output_page,\
        epilog_close_device,\
        gx_default_rgb_map_rgb_color,\
        gx_default_rgb_map_color_rgb,\
        gdev_vector_fill_rectangle,\
        NULL,                   /* tile_rectangle */\
        epilog_copy_mono,			/* copy_mono */\
        NULL,			/* copy_color */\
        NULL,                   /* draw_line */\
        NULL,                   /* get_bits */\
        epilog_get_params,\
        epilog_put_params,\
        NULL,                   /* map_cmyk_color */\
        NULL,                   /* get_xfont_procs */\
        NULL,                   /* get_xfont_device */\
        NULL,                   /* map_rgb_alpha_color */\
        gx_page_device_get_page_device,\
        NULL,                   /* get_alpha_bits */\
        NULL,                   /* copy_alpha */\
        NULL,                   /* get_band */\
        NULL,                   /* copy_rop */\
        epilog_fill_path,\
        epilog_stroke_path,\
        NULL,			/* fill_mask */\
        NULL,/*gdev_vector_fill_trapezoid,*/\
        NULL,/*gdev_vector_fill_parallelogram,*/\
        NULL,/*gdev_vector_fill_triangle,*/\
        NULL,			/* draw_thin_line */\
        NULL, /*gx_default_begin_image,			/ begin_image */\
        NULL,                   /* image_data */\
        NULL,                   /* end_image */\
        NULL,                   /* strip_tile_rectangle */\
        NULL,			/* strip_copy_rop */\
        NULL,			/* get_clipping_box */\
        epilog_begin_typed_image,\
        NULL,			/* get_bits_rectangle */\
        NULL,			/* map_color_rgb_alpha */\
        NULL,			/* create_compositor */\
        NULL			/* get_hardware_params */\
        /*epilog_text_begin*/\
}

gs_public_st_suffix_add0_final(st_device_epilog, gx_device_epilog,
                               "gx_device_epilog",
                               device_epilog_enum_ptrs, device_epilog_reloc_ptrs,
                               gx_device_finalize, st_device_vector);

const gx_device_epilog gs_epilog_device = {
    epilog_device_body("epilog", 24),
    epilog_device_procs
};

/* Vector device procedures */

static int
epilog_beginpage(gx_device_vector *vdev);
static int
epilog_setlinewidth(gx_device_vector *vdev, floatp width);
static int
epilog_setlinecap(gx_device_vector *vdev, gs_line_cap cap);
static int
epilog_setlinejoin(gx_device_vector *vdev, gs_line_join join);
static int
epilog_setmiterlimit(gx_device_vector *vdev, floatp limit);
static int
epilog_setdash(gx_device_vector *vdev, const float *pattern,
            uint count, floatp offset);
static int
epilog_setlogop(gx_device_vector *vdev, gs_logical_operation_t lop,
             gs_logical_operation_t diff);

static int
epilog_can_handle_hl_color(gx_device_vector *vdev, const gs_imager_state *pis,
                        const gx_drawing_color * pdc);
static int
epilog_setfillcolor(gx_device_vector *vdev, const gs_imager_state *pis,
                 const gx_drawing_color *pdc);
static int
epilog_setstrokecolor(gx_device_vector *vdev, const gs_imager_state *pis,
                   const gx_drawing_color *pdc);

static int
epilog_dopath(gx_device_vector *vdev, const gx_path * ppath,
           gx_path_type_t type, const gs_matrix *pmat);

static int
epilog_dorect(gx_device_vector *vdev, fixed x0, fixed y0,
           fixed x1, fixed y1, gx_path_type_t type);
static int
epilog_beginpath(gx_device_vector *vdev, gx_path_type_t type);

static int
epilog_moveto(gx_device_vector *vdev, floatp x0, floatp y0,
           floatp x, floatp y, gx_path_type_t type);
static int
epilog_lineto(gx_device_vector *vdev, floatp x0, floatp y0,
           floatp x, floatp y, gx_path_type_t type);
static int
epilog_curveto(gx_device_vector *vdev, floatp x0, floatp y0,
            floatp x1, floatp y1, floatp x2, floatp y2,
            floatp x3, floatp y3, gx_path_type_t type);
static int
epilog_closepath(gx_device_vector *vdev, floatp x, floatp y,
              floatp x_start, floatp y_start, gx_path_type_t type);
static int
epilog_endpath(gx_device_vector *vdev, gx_path_type_t type);

static int
epilog_make_path_image(gx_device_epilog *epilog, gx_path * ppath, const gx_clip_path * pcpath);

static int
epilog_copy_mono(gx_device * dev, const byte * data,
                     int dx, int raster, gx_bitmap_id id, int x, int y, int w, int h,
                 gx_color_index zero, gx_color_index one);

void tabMaker(gx_device_epilog * epilog);

void increaseIndent(gx_device_epilog * epilog);

void decreaseIndent(gx_device_epilog * epilog);

void epilog_write_tabbed(gx_device_epilog * epilog,const char* str);

void strokeString(gx_device_epilog *epilog,char* str,gx_path_type_t type);

png_byte** makePNG(int height,int width,int depth);

void freePNG(png_byte** arr,int height,int width);

void user_write_data(png_structp png_ptr, png_bytep data, png_size_t length);

void user_flush_data(png_structp png_ptr);

/* Vector device function table */

static const gx_device_vector_procs epilog_vector_procs = {
        /* Page management */
    epilog_beginpage,
        /* Imager state */
    epilog_setlinewidth,
    epilog_setlinecap,
    epilog_setlinejoin,
    epilog_setmiterlimit,
    epilog_setdash,
    gdev_vector_setflat,
    epilog_setlogop,
        /* Other state */
    epilog_can_handle_hl_color,
    epilog_setfillcolor,
    epilog_setstrokecolor,
        /* Paths */
    gdev_vector_dopath,
    epilog_dorect,
    epilog_beginpath,
    epilog_moveto,
    epilog_lineto,
    epilog_curveto,
    epilog_closepath,
    epilog_endpath
};

/* local utility prototypes */

static int epilog_write_bytes(gx_device_epilog *epilog,
                const char *string, uint length);
static int epilog_write(const gx_device_epilog *epilog, const char *string);

static int epilog_write_header(gx_device_epilog *epilog);

static int epilog_write_base64(gx_device_epilog *epilog, const char *string, uint length);

void epilog_reverse_path_list(gx_cpath_path_list **head);

static int epilog_make_png_image(gx_device_epilog *epilog, int w, int h);

/* global variables */
png_bytep rawPNG;
size_t rawPNGLength;

/* ---------------- Internal utilities ---------------- */

void epilog_reverse_path_list(gx_cpath_path_list **head) {
    gx_cpath_path_list *first;
    gx_cpath_path_list *rest;
    
    if(*head == NULL)
        return;
    
    first = *head;
    rest = first->next;
    
    if(rest == NULL)
        return;
    
    epilog_reverse_path_list(&rest);
    
    first->next->next = first;
    first->next = NULL;
    
    *head = rest;
}

static void
epilog_drawing_color(gx_device_epilog *epilog, const char *prefix, const gx_drawing_color *pdcolor)
{
    char line[EPILOG_LINESIZE];
    const gs_memory_t *mem = epilog->memory;
    dmprintf1(mem, "%scolor=", prefix);
    epilog_write(epilog, "color=");
    if (pdcolor->type == gx_dc_type_none) {
        dmputs(mem, "none");
        epilog_write(epilog, "'none'");
        epilog->colorOK = true;
    }
    else if (pdcolor->type == gx_dc_type_null) {
        dmputs(mem, "null");
        epilog_write(epilog, "'null'");
        epilog->colorOK = true;
    }
    else if (pdcolor->type == gx_dc_type_pure) {
        dmprintf1(mem, "0x%lx", (ulong)pdcolor->colors.pure);
        sprintf(line, "'0x%lx'",(ulong)pdcolor->colors.pure);
        epilog_write(epilog, line);
        epilog->colorOK = true;
    }
    else if (pdcolor->type == gx_dc_type_ht_binary) {
        int ci = pdcolor->colors.binary.b_index;
        
        dmprintf5(mem, "binary(0x%lx, 0x%lx, %d/%d, index=%d)",
                  (ulong)pdcolor->colors.binary.color[0],
                  (ulong)pdcolor->colors.binary.color[1],
                  pdcolor->colors.binary.b_level,
                  (ci < 0 ? pdcolor->colors.binary.b_ht->order.num_bits :
                   pdcolor->colors.binary.b_ht->components[ci].corder.num_bits),
                  ci);
        epilog_write(epilog, "'binary'");
        epilog->colorOK = false;
    } else if (pdcolor->type == gx_dc_type_ht_colored) {
        ulong plane_mask = pdcolor->colors.colored.plane_mask;
        int ci;
        epilog_write(epilog, "'htColor'");
        dmprintf1(mem, "colored(mask=%lu", plane_mask);
        for (ci = 0; plane_mask != 0; ++ci, plane_mask >>= 1)
            if (plane_mask & 1) {
                dmprintf2(mem, ", (base=%u, level=%u)",
                          pdcolor->colors.colored.c_base[ci],
                          pdcolor->colors.colored.c_level[ci]);
            } else
                dmputs(mem, ", -");
        epilog->colorOK = false;
    } else if(pdcolor->type == gx_dc_type_pattern) {
        dmputs(mem, "**gx_dc_type_pattern**");
        epilog->colorOK = false;
    }else if (pdcolor->type == gx_dc_type_devn) {
        dmputs(mem, "**gx_dc_type_devn**");
        epilog->colorOK = false;
    } else if(pdcolor->type == gx_dc_type_pattern2) {
        dmputs(mem, "**gx_dc_type_pattern2**");
        epilog->colorOK = false;
    }
    else {
        dmputs(mem, "**unknown**");
        epilog_write(epilog, "'unknown'");
        epilog->colorOK = false;
    }
}

static bool
epilog_valid_color(gx_device_epilog *epilog, const char *prefix, const gx_drawing_color *pdcolor)
{
    if (pdcolor->type == gx_dc_type_none) {
        return true;
    }
    else if (pdcolor->type == gx_dc_type_null) {
        return true;
    }
    else if (pdcolor->type == gx_dc_type_pure) {
        return true;
    }
    else {
        return false;
    }
}

/* Implement copy_mono by filling lots of small rectangles. */
/* This is very inefficient, but it works as a default. */
static int
epilog_copy_mono(gx_device * dev, const byte * data,
                     int dx, int raster, gx_bitmap_id id, int x, int y, int w, int h,
                     gx_color_index zero, gx_color_index one)
{
    int code;
    char line[EPILOG_LINESIZE];
    gx_device_epilog *epilog = (gx_device_epilog*) dev;
    if (!(w&&h)) {
        // There is not data for this character b/c the width or height is zero
        return 0;
    }
    // Initialize the PNG stuff
    char *encodedPNG;
    size_t encodedPNGLength;
    gs_fixed_point p;
    p.x = int2fixed(x);
    p.y = int2fixed(y);
    epilog->offsetPoint = p;
    epilog->padding = 0;
     dmprintf4(dev->memory, "Copy mono: %d,%d,%d,%d\n", x, y, w, h);
    rawPNG = 0;
    rawPNGLength = 0;
    epilog_make_png_image(epilog, w, h);
    epilog->row_pointers = makePNG(h,w, 4);
    
    code = gx_default_copy_mono(dev, data, dx, raster, id, x, y, w, h, zero, one);
    // Finish the PNG stuff
    /* write the rest of the file */
    png_write_image(epilog->png_struct, epilog->row_pointers);
    png_write_end(epilog->png_struct,epilog->png_info);
    
#if PNG_LIBPNG_VER_MINOR >= 5
#else
    /* if you alloced the palette, free it here */
    gs_free_object(mem, epilog->palettep, "png palette");
#endif
    // Free png data
    freePNG(epilog->row_pointers, h, w);
    
    // Encode data
    encodedPNG = base64_encode(rawPNG, rawPNGLength, &encodedPNGLength);
    
    // Write data to file
    epilog_write_tabbed(epilog, "<pathImage");
    sprintf(line, " x='%d' y='%d' encoding='png/base64'>\n",x,y);
    epilog_write(epilog, line);
    increaseIndent(epilog);
    epilog_write_base64(epilog, encodedPNG, encodedPNGLength);
    decreaseIndent(epilog);
    epilog_write_tabbed(epilog, "</pathImage>\n");
    
    // Free data and encoded data
    free(rawPNG);
    free(encodedPNG);
    
done:
    /* free the structures */
    png_destroy_write_struct(&epilog->png_struct, &epilog->png_info);
    return code;
}

static void
epilog_path(gx_device_epilog *epilog, const gx_path *path)
{
    char line[EPILOG_LINESIZE];
    gs_path_enum penum;
    gx_path_enum_init(&penum, path);
    for (;;) {
        gs_fixed_point pts[3];
        
        switch (gx_path_enum_next(&penum, pts)) {
            case gs_pe_moveto:
                dmprintf2(path->memory,"    %g %g moveto\n", fixed2float(pts[0].x),
                          fixed2float(pts[0].y));
                epilog_write_tabbed(epilog, "<moveto>");
                sprintf(line, "%f,%f</moveto>\n",fixed2float(pts[0].x),fixed2float(pts[0].y));
                epilog_write(epilog,line);
                continue;
            case gs_pe_lineto:
                dmprintf2(path->memory,"    %g %g lineto\n", fixed2float(pts[0].x),
                          fixed2float(pts[0].y));
                epilog_write_tabbed(epilog, "<lineto>");
                sprintf(line, "%f,%f</lineto>\n",fixed2float(pts[0].x),fixed2float(pts[0].y));
                epilog_write(epilog,line);
                continue;
            case gs_pe_gapto:
                dmprintf2(path->memory,"    %g %g gapto\n", fixed2float(pts[0].x),
                          fixed2float(pts[0].y));
                epilog_write_tabbed(epilog, "<gapto>");
                sprintf(line, "%f,%f</gapto>\n",fixed2float(pts[0].x),fixed2float(pts[0].y));
                epilog_write(epilog,line);
                continue;
            case gs_pe_curveto:
                dmprintf6(path->memory,"    %g %g %g %g %g %g curveto\n", fixed2float(pts[0].x),
                          fixed2float(pts[0].y), fixed2float(pts[1].x),
                          fixed2float(pts[1].y), fixed2float(pts[2].x),
                          fixed2float(pts[2].y));
                epilog_write_tabbed(epilog, "<curveto>");
                sprintf(line, "%f,%f %f,%f %f,%f</curveto>\n",
                        fixed2float(pts[0].x),fixed2float(pts[0].y),
                        fixed2float(pts[1].x), fixed2float(pts[1].y),
                        fixed2float(pts[2].x),fixed2float(pts[2].y));
                epilog_write(epilog,line);
                continue;
            case gs_pe_closepath:
                dmputs(path->memory,"    closepath\n");
                epilog_write_tabbed(epilog, "<closepath/>\n");
                continue;
            default:
                break;
        }
        break;
    }
}

static void
epilog_clip(const gx_device_epilog *epilog, const gx_clip_path *pcpath)
{
    if (pcpath == 0)
        return;
    if (gx_cpath_includes_rectangle(pcpath, fixed_0, fixed_0,
                                    int2fixed(epilog->width),
                                    int2fixed(epilog->height))
        )
        return;
    dmputs(epilog->memory, ", clip={");
    if (pcpath->path_valid)
        epilog_path(epilog,&pcpath->path);
    else
        dmputs(epilog->memory, "NO PATH");
    dmputs(epilog->memory, "}");
}

/* Driver procedure implementation */

/* Open the device */
static int
epilog_open_device(gx_device *dev)
{
    gx_device_vector *const vdev = (gx_device_vector*)dev;
    gx_device_epilog *const epilog = (gx_device_epilog*)dev;
    int code = 0;

    vdev->v_memory = dev->memory;
    vdev->vec_procs = &epilog_vector_procs;
    gdev_vector_init(vdev);
    code = gdev_vector_open_file_options(vdev, 512,
        VECTOR_OPEN_FILE_SEQUENTIAL);
    if (code < 0)
      return gs_rethrow_code(code);

    /* epilog-specific initialization goes here */
    epilog->header = 0;
    epilog->dirty = 0;
    epilog->mark = 0;
    epilog->page_count = 0;
    epilog->strokecolor = gx_no_color_index;
    epilog->fillcolor = gx_no_color_index;
    /* these should be the graphics library defaults instead? */
    epilog->linewidth = EPILOG_DEFAULT_LINEWIDTH;
    epilog->startcap = EPILOG_DEFAULT_LINECAP;
    epilog->linejoin = EPILOG_DEFAULT_LINEJOIN;
    epilog->miterlimit = EPILOG_DEFAULT_MITERLIMIT;
    epilog->rasterLayers = 0;
    epilog->tabLevel = 0;
    epilog->streamFName.data = 0;
    epilog->streamFName.size = 0;
    epilog->pathLevel = 0;
    epilog->numImages = 0;
    epilog->numPaths = 0;
    epilog->csvOpen = false;
    return code;
}

/* Functions for manipulation params strings */

static const byte*
paramValueToString(const stringParamDescription* params, int value)
{
    
    for (; params->p_name; ++params) {
        if (params->p_value == value) {
            return (const byte *)params->p_name;
        }
    }
    
    return (const byte*) 0;
}

static int
paramStringValue(const stringParamDescription* params,
                 const byte* name, int namelen, int* value)
{
    
    for (; params->p_name; ++params) {
        if (strncmp(params->p_name, (char *)name, namelen) == 0 &&
            params->p_name[namelen] == 0) {
            *value = params->p_value;
            return 1;
        }
    }
    
    return 0;
}

static int
put_param_string(gs_param_list* plist,
                 const byte* pname, gs_param_string* pstring,
                 const stringParamDescription* params, int *pvalue, int code)
{
    
    int ncode;
    
    if ((ncode = param_read_string(plist, (char *)pname, pstring)) < 0) {
        param_signal_error(plist, (char *)pname, code = ncode);
    } else if (ncode == 1) {
        pstring->data = 0, pstring->size = 0;
    } else {
        int value = 0;
        
        if (paramStringValue(params, pstring->data, pstring->size,
                             &value) == 0) {
            param_signal_error(plist, (char *)pname, code = gs_error_rangecheck);
        } else {
            *pvalue = value;
        }
    }
    
    return code;
}

static int
get_param_string(gs_param_list* plist,
                 const byte* pname, gs_param_string* pstring,
                 const stringParamDescription* params, int pvalue, bool persist, int code)
{
    
    int ncode;
    
    pstring->data = paramValueToString(params, pvalue);
    
    if (pstring->data == (byte*) 0) {
        param_signal_error(plist, (char *)pname, ncode = gs_error_unknownerror);
    } else {
        pstring->size = strlen((char *)pstring->data);
        pstring->persistent = persist;
    }
    
    if ((ncode = param_write_string(plist, (char *)pname, pstring)) < 0) {
        code = ncode;
    }
    
    return code;
}


/* Complete a page */
static int
epilog_output_page(gx_device *dev, int num_copies, int flush)
{
    gx_device_epilog *const epilog = (gx_device_epilog*)dev;
    int code;
#if PRINT_FUNCTION
    dmputs(dev->memory,"In epilog_output_page\n");
#endif
    epilog->page_count++;

    if (ferror(epilog->file))
      return gs_throw_code(gs_error_ioerror);

    if ((code=gx_finish_output_page(dev, num_copies, flush)) < 0)
        return code;
    /* Check if we need to change the output file for separate pages */
    if (gx_outputfile_is_separate_pages(((gx_device_vector *)dev)->fname, dev->memory)) {
        if ((code = epilog_close_device(dev)) < 0)
            return code;
        code = epilog_open_device(dev);
    }
    return code;
}

/* Close the device */
static int
epilog_close_device(gx_device *dev)
{
    gx_device_epilog *const epilog = (gx_device_epilog*)dev;
    char line[EPILOG_LINESIZE];
#if PRINT_FUNCTION
    dmputs(dev->memory,"In epilog_close_device\n");
#endif
    
    /* close any open group elements */
    while (epilog->mark > 0) {
      epilog_write(epilog, "</g>\n");
      epilog->mark--;
    }
    while (epilog->pathLevel > 0) {
        epilog->tabLevel--;
        tabMaker(epilog);
        sprintf(line, "%s</path>\n",epilog->tabs);
        epilog_write(epilog, line);
        epilog->pathLevel--;
        
    }
    if (epilog->header) {
        epilog->tabLevel = 0;
        tabMaker(epilog);
        epilog_write(epilog, "</page>\n");
        epilog->header = 0;
    }
    
    if (ferror(epilog->file))
      return gs_throw_code(gs_error_ioerror);

    return gdev_vector_close_file((gx_device_vector*)dev);
}

static stringParamDescription epg_ditheringTypeStrings[] = {
    { "standard",			EPG_DITHER_STD },
    { "brighten",			EPG_DITHER_BRIGHTEN },
    { "lowres",             EPG_DITHER_LOWRES },
    { "floyd",              EPG_DITHER_FLOYD },
    { "jarvis",             EPG_DITHER_JARVIS },
    { "stucki",             EPG_DITHER_STUCKI },
    { 0 }
};

static stringParamDescription epg_jobTypeStrings[] = {
    { "mono",			EPG_JOB_MONO },
    { "3D",			EPG_JOB_3D },
    { "Color",             EPG_JOB_COLOR },
    { 0 }
};

//static stringParamDescription bjc_ditheringTypeStrings[] = {
//    { "standard",			EPG_DITHER_STD },
//    { "brighten",			EPG_DITHER_BRIGHTEN },
//    { "lowres",             EPG_DITHER_LOWRES },
//    { "floyd",              EPG_DITHER_FLOYD },
//    { "jarvis",             EPG_DITHER_JARVIS },
//    { "stucki",             EPG_DITHER_STUCKI },
//    { 0 }
//};

static int
epilog_get_params(gx_device *pdev, gs_param_list *plist)
{
    int code = gdev_vector_get_params(pdev, plist);
    gx_device_epilog* epilogDev = (gx_device_epilog*)pdev;
#if PRINT_FUNCTION
    dmputs(pdev->memory,"In epilog_get_params\n");
#endif
    
    gs_param_string dithering;
    gs_param_string jobType;
    
    if (code < 0) return_error(code);
    
    code = get_param_string(plist, (unsigned char *)EPG_OPTION_DITHERINGTYPE, &dithering,
                            epg_ditheringTypeStrings, epilogDev->params.ditheringType, true, code);
    
    code = get_param_string(plist, (unsigned char *)EPG_OPTION_JOBTYPE, &jobType,
                            epg_jobTypeStrings, epilogDev->params.jobType, true, code);
    
    
    return code;
}

/* Put properties for the epilog driver. */

static int
epilog_put_params(gx_device *pdev, gs_param_list *plist)
{    
    int code = 0;
    int ncode;
    epg_params new800Params;
    epg_params* params;
    gs_param_string dithering;
    gs_param_string jobType;
    gx_device_epilog* epilogDev = (gx_device_epilog*)pdev;
#if PRINT_FUNCTION
    dmputs(pdev->memory,"In epilog_put_params\n");
#endif
    
    new800Params = epilogDev->params;
    params = (epg_params*) &new800Params;
    
    code = put_param_string(plist, (unsigned char *)EPG_OPTION_DITHERINGTYPE, &dithering,
                            epg_ditheringTypeStrings, &params->ditheringType, code);
    
    code = put_param_string(plist, (unsigned char *)EPG_OPTION_JOBTYPE, &jobType,
                            epg_jobTypeStrings, &params->jobType, code);
    
    
    if ((ncode = gdev_vector_put_params(pdev, plist)) < 0) {
        code = ncode;
    }
    
    if (code < 0)
        return code;
    
    /* Write values that did change */
    epilogDev->params = new800Params;
    return code;
}

/* write a length-limited char buffer */
static int
epilog_write_bytes(gx_device_epilog *epilog, const char *string, uint length)
{
    /* calling the accessor ensures beginpage is called */
    stream *s = gdev_vector_stream((gx_device_vector*)epilog);
    uint used;

    sputs(s, (const byte *)string, length, &used);

    return !(length == used);
}

/* write a length-limited char buffer with max 80 char per line */
static int
epilog_write_base64(gx_device_epilog *epilog, const char *string, uint length)
{
    // Break the image into 80 character lines
    int i,start,len;
    int lines = length/80;
    // Add a line if there is some remainder
    if((length % 80) > 0)
        lines++;
    
    for(i = 0; i < lines;i++)
    {
        start = i*80;
        len = (length-start) > 80 ? 80 : length-start;
        epilog_write(epilog,epilog->tabs);
        epilog_write_bytes(epilog, &string[start], len);
        epilog_write_bytes(epilog, "\n", 1);
    }
    return 0;
}

/* write a null terminated string */
static int
epilog_write(const gx_device_epilog *epilog, const char *string)
{
    return epilog_write_bytes(epilog, string, strlen(string));
}

static int
epilog_write_header(gx_device_epilog *epilog)
{
    /* we're called from beginpage, so we can't use
       epilog_write() which calls gdev_vector_stream()
       which calls beginpage! */
    stream *s = epilog->strm;
    uint used;
    char line[300];

    if_debug0m('_', epilog->memory, "epilog_write_header\n");

    /* only write the header once */
    if (epilog->header)
      return 1;

    /* write the initial boilerplate */
    sprintf(line, "%s\n", XML_DECL);
    sputs(s, (byte *)line, strlen(line), &used);
    sprintf(line, "<page>\n");
    increaseIndent(epilog);
    sputs(s, (byte *)line, strlen(line), &used);
    sprintf(line, "%s<BoundingBox>\n",epilog->tabs);
    sputs(s, (byte *)line, strlen(line), &used);
    increaseIndent(epilog);
    sprintf(line, "%s<width>%d</width>\n",epilog->tabs,epilog->width);
    sputs(s, (byte *)line, strlen(line), &used);
    sprintf(line, "%s<height>%d</height>\n",epilog->tabs,epilog->height);
    sputs(s, (byte *)line, strlen(line), &used);
    decreaseIndent(epilog);
    sprintf(line, "%s</BoundingBox>\n",epilog->tabs);
    sputs(s, (byte *)line, strlen(line), &used);

    //epilog->mark++;

    /* mark that we've been called */
    epilog->header = 1;

    return 0;
}

static gx_color_index
epilog_get_color(gx_device_epilog *epilog, const gx_drawing_color *pdc)
{

    gx_color_index color = gx_no_color_index;
#if PRINT_FUNCTION
    dmputs(epilog->memory,"In epilog_get_color\n");
#endif
    if (gx_dc_is_pure(pdc))
	color = gx_dc_pure_color(pdc);
    return color;
}

static int
epilog_write_state(gx_device_epilog *epilog)
{
    char line[EPILOG_LINESIZE];
#if PRINT_FUNCTION
    dmputs(epilog->memory,"In epilog_write_state\n");
#endif
    /* has anything changed? */
    if (!epilog->dirty)
      return 0;

    /* close the current graphics state element, if any */
    if (epilog->mark > 1) {
      epilog_write(epilog, "</g>\n");
      epilog->mark--;
    }
    /* write out the new current state */
    epilog_write(epilog, "<g ");
    if (epilog->strokecolor != gx_no_color_index) {
	sprintf(line, " stroke='#%06lx'", epilog->strokecolor & 0xffffffL);
        epilog_write(epilog, line);
    } else {
        epilog_write(epilog, " stroke='none'");
    }
    if (epilog->fillcolor != gx_no_color_index) {
        sprintf(line, " fill='#%06lx'", epilog->fillcolor & 0xffffffL);
        epilog_write(epilog, line);
    } else {
      epilog_write(epilog, " fill='none'");
    }
    if (epilog->linewidth != 1.0) {
      sprintf(line, " stroke-width='%lf'", epilog->linewidth);
      epilog_write(epilog, line);
    }
    if (epilog->startcap != EPILOG_DEFAULT_LINECAP) {
        switch (epilog->startcap) {
          case gs_cap_round:
            epilog_write(epilog, " stroke-linecap='round'");
            break;
          case gs_cap_square:
            epilog_write(epilog, " stroke-linecap='square'");
            break;
          case gs_cap_butt:
          default:
            /* treat all the other options as the default */
            epilog_write(epilog, " stroke-linecap='butt'");
            break;
        }
    }
    if (epilog->linejoin != EPILOG_DEFAULT_LINEJOIN) {
        switch (epilog->linejoin) {
          case gs_join_round:
            epilog_write(epilog, " stroke-linejoin='round'");
            break;
          case gs_join_bevel:
            epilog_write(epilog, " stroke-linejoin='bevel'");
            break;
          case gs_join_miter:
          default:
            /* EPILOG doesn't support any other variants */
            epilog_write(epilog, " stroke-linejoin='miter'");
            break;
        }
    }
    if (epilog->miterlimit != EPILOG_DEFAULT_MITERLIMIT) {
        sprintf(line, " stroke-miterlimit='%lf'", epilog->miterlimit);
        epilog_write(epilog, line);
    }
    epilog_write(epilog, ">\n");
    epilog->mark++;

    epilog->dirty = 0;
    return 0;
}

/* vector device implementation */

        /* Page management */
static int
epilog_beginpage(gx_device_vector *vdev)
{
    gx_device_epilog *epilog = (gx_device_epilog *)vdev;

    epilog_write_header(epilog);
#if PRINT_FUNCTION
    dmputs(vdev->memory,"In epilog_beginpage\n");
#endif
    
    if_debug1m('_', epilog->memory, "epilog_beginpage (page count %d)\n", epilog->page_count);
    return 0;
}

        /* Imager state */
static int
epilog_setlinewidth(gx_device_vector *vdev, floatp width)
{
    gx_device_epilog *epilog = (gx_device_epilog *)vdev;

    if_debug1m('_', epilog->memory, "epilog_setlinewidth(%lf)\n", width);
#if PRINT_FUNCTION
    dmputs(vdev->memory,"In epilog_setlinewidth\n");
#endif
    epilog->linewidth = width;
    epilog->dirty++;

    return 0;
}
static int
epilog_setlinecap(gx_device_vector *vdev, gs_line_cap cap)
{
    gx_device_epilog *epilog = (gx_device_epilog *)vdev;
    const char *linecap_names[] = {"butt", "round", "square",
        "triangle", "unknown"};

    if (cap < 0 || cap > gs_cap_unknown)
        return gs_throw_code(gs_error_rangecheck);
    if_debug1m('_', epilog->memory, "epilog_setlinecap(%s)\n", linecap_names[cap]);
#if PRINT_FUNCTION
    dmputs(vdev->memory,"In epilog_setlinecap\n");
#endif
    
    epilog->startcap = cap;
    epilog->dirty++;

    return 0;
}
static int
epilog_setlinejoin(gx_device_vector *vdev, gs_line_join join)
{
    gx_device_epilog *epilog = (gx_device_epilog *)vdev;
    const char *linejoin_names[] = {"miter", "round", "bevel",
        "none", "triangle", "unknown"};

    if (join < 0 || join > gs_join_unknown)
        return gs_throw_code(gs_error_rangecheck);
    if_debug1m('_', epilog->memory, "epilog_setlinejoin(%s)\n", linejoin_names[join]);
#if PRINT_FUNCTION
    dmputs(vdev->memory,"In epilog_setlinejoin\n");
#endif

    epilog->linejoin = join;
    epilog->dirty++;

    return 0;
}
static int
epilog_setmiterlimit(gx_device_vector *vdev, floatp limit)
{
    if_debug1m('_', vdev->memory, "epilog_setmiterlimit(%lf)\n", limit);
    return 0;
}
static int
epilog_setdash(gx_device_vector *vdev, const float *pattern,
            uint count, floatp offset)
{
    if_debug0m('_', vdev->memory, "epilog_setdash\n");
    return 0;
}
static int
epilog_setlogop(gx_device_vector *vdev, gs_logical_operation_t lop,
             gs_logical_operation_t diff)
{
    if_debug2m('_', vdev->memory, "epilog_setlogop(%u,%u) set logical operation\n",
        lop, diff);
    /* EPILOG can fake some simpler modes, but we ignore this for now. */
    return 0;
}

        /* Other state */

static int
epilog_can_handle_hl_color(gx_device_vector *vdev, const gs_imager_state *pis,
                          const gx_drawing_color * pdc)
{
    if_debug0m('_', vdev->memory, "epilog_can_handle_hl_color\n");
    return 0;
}

static int
epilog_setfillcolor(gx_device_vector *vdev, const gs_imager_state *pis,
                 const gx_drawing_color *pdc)
{
    gx_device_epilog *epilog = (gx_device_epilog*)vdev;
    gx_color_index fill = epilog_get_color(epilog, pdc);
    int pWidth,pHeight,xdpi,ydpi;

    if_debug0m('_', epilog->memory, "epilog_setfillcolor\n");
#if PRINT_FUNCTION
    dmputs(vdev->memory,"In epilog_setfillcolor\n");
#endif
    pWidth = epilog->MediaSize[0];
    pHeight = epilog->MediaSize[1];
    xdpi = epilog->x_pixels_per_inch;
    ydpi = epilog->y_pixels_per_inch;

    if (epilog->fillcolor == fill)
      return 0; /* not a new color */
    
    
    /* update our state with the new color */
    epilog->fillcolor = fill;
    /* request a new group element */
    epilog->dirty++;
    return 0;
}

static int
epilog_setstrokecolor(gx_device_vector *vdev, const gs_imager_state *pis,
                   const gx_drawing_color *pdc)
{
    gx_device_epilog *epilog = (gx_device_epilog*)vdev;
    gx_color_index stroke = epilog_get_color(epilog, pdc);
    

    if_debug0m('_', epilog->memory, "epilog_setstrokecolor\n");
#if PRINT_FUNCTION
    dmputs(vdev->memory,"In epilog_setstrokecolor\n");
#endif
    
    if (epilog->strokecolor == stroke)
      return 0; /* not a new color */

    /* update our state with the new color */
    epilog->strokecolor = stroke;
    /* request a new group element */
    epilog->dirty++;

    return 0;
}

        /* Paths */
/*    gdev_vector_dopath */

static int epilog_print_path_type(gx_device_epilog *epilog, gx_path_type_t type)
{
    const char *path_type_names[] = {"winding number", "fill", "stroke",
        "fill and stroke", "clip"};

    if (type <= 4)
        if_debug2m('_', epilog->memory, "type %d (%s)", type, path_type_names[type]);
    else
        if_debug1m('_', epilog->memory, "type %d", type);

    return 0;
}

static int
epilog_dopath(gx_device_vector *vdev, const gx_path * ppath,
           gx_path_type_t type, const gs_matrix *pmat)
{
    // Insert code here
    // Hack to make the tab levels work
    gx_device_epilog *epilog = (gx_device_epilog *)vdev;
    //char line[EPILOG_LINESIZE];
    //char strokeLine[EPILOG_LINESIZE];
    epilog_write(epilog, "");
#if PRINT_FUNCTION
    dmputs(vdev->memory,"In epilog_dopath\n");
#endif
    
    
    if(type == gx_path_type_clip)
        dlprintf("Clip path epilog_dopath\n");
    
    return gdev_vector_dopath(vdev, ppath, type, pmat);
}

static int
epilog_dorect(gx_device_vector *vdev, fixed x0, fixed y0,
           fixed x1, fixed y1, gx_path_type_t type)
{
    gx_device_epilog *epilog = (gx_device_epilog *)vdev;
    char line[EPILOG_LINESIZE];
    int w,h,i,s,f,j;
    int pWidth,pHeight,xdpi,ydpi;
    int p[2][2];
    char* fileName;
    ep_image* layer;
    pWidth = epilog->MediaSize[0];
    pHeight = epilog->MediaSize[1];
    xdpi = epilog->x_pixels_per_inch;
    ydpi = epilog->y_pixels_per_inch;
    
    p[0][0] = fixed2int_var_pixround(x0);
    p[1][0] = fixed2int_var_pixround(x1);
    p[0][1] = fixed2int_var_pixround(y0);
    p[1][1] = fixed2int_var_pixround(y1);
    w = fixed2int_var_pixround(x1 - x0);
    h = fixed2int_var_pixround(y1 - y0);
    
#if PRINT_FUNCTION
    dmputs(vdev->memory,"In epilog_dorect\n");
#endif
    
    if (xdpi != ydpi) {
        //TODO We have some sort of error here
        dmputs(vdev->memory,"xdpi does not equal ydpi in dorect\n");
    }
    
    if(type == gx_path_type_clip)
        dlprintf("Clip path epilog_dorect\n");
    
    /* hack single-page output */
    if (epilog->page_count)
      return 0;
    
//    //Get a raster buffer for this raster line    
//    if (epilog->rasterLayers == 0 && h == 1) {
//        epilog->rasterLayers = make_ep_image(vdev, epilog->lastID);
//    }
//    
//    if(h == 1)
//    {
//        layer = search_ep_image(epilog->rasterLayers, epilog->lastID);
//    }
//    
//    if(layer == 0 && h == 1)
//    {
//        //The layer does not exist so make it
//        epilog->rasterLayers = add_ep_image(epilog->rasterLayers, vdev, epilog->lastID);
//        //Now get it
//        layer = search_ep_image(epilog->rasterLayers, epilog->lastID);
//    }
//    
//        
////        if(w > 1 || h > 1)
////        {
////            if_debug4m('|', epilog->memory, "We are making a box at: (%d,%d) of size: (%d,%d)\n",p[0][0],p[0][1],w,h);
////        }
//    if(h > 1)
//    {
//        //This is bad, we are only setup to handle scanlines so if the height is more than 1 we are in trouble
//        if_debug4m('|', epilog->memory, "We are making a box at: (%d,%d) of size: (%d,%d)\n",p[0][0],p[0][1],w,h);
//    }

//    if(h == 1 && p[0][1] != layer->thisRow )
//    {
//        //We have started a new row so switch buffers and write out the last row
//        fileName = dump_row_ep_image(layer);
//        if (!layer->dumped && fileName) {
//            epilog_write(epilog,""); //Ensure the header has been built
//            tabMaker(epilog);
//            sprintf(line, "%s<raster>%s</raster>\n",epilog->tabs,layer->fileName);
//            epilog_write(epilog,line);
//            layer->dumped = 1;
//        }
//        layer->thisRow = p[0][1];
//        //Switch active row
//        layer->activeRow = layer->activeRow ? 0 : 1;
//        //Clear the row for use
//        memset(layer->image[layer->activeRow], 0x00, layer->width*sizeof(int));
//    }
    
//    s = p[0][0];
//    f = p[1][0];
//    //This is a raster pixle
//    if (h == 1 ) {
//        for (i = p[0][0]; i < p[1][0]; i++) {
//            //dump the color
//            layer->image[layer->activeRow][i] = 0xff000000 | epilog->fillcolor;
//        }
//    }
    if(0 && !epilog->csvOpen)
    {
        if((epilog->fpcsv = fopen("raster.csv","w")))
            epilog->csvOpen = true;
    }
    if(0 && epilog->csvOpen)
    {
        // x y w h color
        for (i = 0; i < w; i++) {
            for(j = 0; j < h; j++) {
                sprintf(line, "%d,%d,%ld\n",s+i,p[0][1]+j,epilog->fillcolor);
                fwrite(line, sizeof(char), strlen(line), epilog->fpcsv);
            }
        }
    }
    
    // dump pixles into png
    int xoffset,yoffset,color;
    xoffset = fixed2int_pixround_perfect(x0-epilog->offsetPoint.x)+epilog->padding;
    yoffset = fixed2int_pixround_perfect(y0-epilog->offsetPoint.y)+epilog->padding;
    for (i = 0; i < w ; i++) {
        for(j = 0; j < h; j++) {
            // Reverse the color for storage
            color = 0;
            color |= epilog->fillcolor & 0xff00;
            color |= ((int)(epilog->fillcolor & 0xff0000) >> 16);
            color |= ((int)(epilog->fillcolor & 0xff) << 16);
            ((int *)epilog->row_pointers[j+yoffset])[i+xoffset] = 0xff000000 | color;
        }
    }

    if_debug0m('_', epilog->memory, "epilog_dorect");
    epilog_print_path_type(epilog, type);
    if_debug0m('_', epilog->memory, "\n");
    
    /* Only draw an epilog rect if it is of type stroke with a color */
    if (type & gx_path_type_stroke)
    {
        //epilog_write_state(epilog);

        if (type & gx_path_type_clip) {
            return 0;
            epilog_write(epilog, "<clipPath>\n");
        }
        tabMaker(epilog);
        sprintf(line, "%s<rect x='%lf' y='%lf' width='%lf' height='%lf'", epilog->tabs,
            fixed2float(x0), fixed2float(y0),
            fixed2float(x1 - x0), fixed2float(y1 - y0));
        
        epilog_write(epilog, line);
        
        /* override the inherited stroke attribute if we're not stroking */
        if ((epilog->strokecolor == gx_no_color_index))
            epilog_write(epilog, " stroke='none'");
        else if(type & gx_path_type_stroke)
        {
            sprintf(line," stroke='#%06lx'",epilog->strokecolor & 0xffffffL);
            epilog_write(epilog, line);
        }
        
        if (type & gx_path_type_stroke) {
            sprintf(line, " stroke-width='%lf'", epilog->linewidth);
            epilog_write(epilog,line);
        }
            
        /* override the inherited fill attribute if we're not filling */
        if ((epilog->fillcolor == gx_no_color_index))
            epilog_write(epilog, " fill='none'");
        else if(type & gx_path_type_fill)
        {
            sprintf(line," fill='#%06lx'",epilog->fillcolor & 0xffffffL);
            epilog_write(epilog, line);
        }
        
        if (type & gx_path_type_even_odd) {
            epilog_write(epilog, " fillRule='evenOdd'");
        } else {
            epilog_write(epilog, " fillRule='winding'");
        }
        
        epilog_write(epilog, "/>\n");

        if (type & gx_path_type_clip) {
            epilog_write(epilog, "<clipPath/>\n");
        }
    }

    return 0;
}

static int
epilog_beginpath(gx_device_vector *vdev, gx_path_type_t type)
{
    gx_device_epilog *epilog = (gx_device_epilog *)vdev;
    char line[EPILOG_LINESIZE];
    char strokeLine[EPILOG_LINESIZE];
    epilog_write(epilog, "");
    
#if PRINT_FUNCTION
    dmputs(vdev->memory,"In epilog_beginpath\n");
#endif
    return 0;
    /* hack single-page output */
    if (epilog->page_count)
      return 0;
    
    if(type == gx_path_type_clip)
        dlprintf("Clip path epilog_beginpath\n");
    
    tabMaker(epilog);
    strokeString(epilog, strokeLine,type);
    sprintf(line, "%s<clippath/>\n", epilog->tabs);
    epilog_write(epilog, line);

    /* skip non-drawing paths for now */
    if (!(type & gx_path_type_fill) && !(type & gx_path_type_stroke))
    {
        dlprintf("Skipping a path? epilog_beginpath\n");
      return 0;
    }

    if_debug0m('_', epilog->memory, "epilog_beginpath ");
    epilog_print_path_type(epilog, type);
    if_debug0m('_', epilog->memory, "\n");

    //epilog_write_state(epilog);
    // Hack to make the tab levels work
    epilog_write(epilog, "");
    tabMaker(epilog);
    strokeString(epilog, strokeLine,type);
    sprintf(line, "%s<path %s>\n", epilog->tabs, strokeLine);
    epilog_write(epilog, line);
    epilog->tabLevel++;
    epilog->pathLevel++;

    return 0;
}

static int
epilog_moveto(gx_device_vector *vdev, floatp x0, floatp y0,
           floatp x, floatp y, gx_path_type_t type)
{
    gx_device_epilog *epilog = (gx_device_epilog *)vdev;
    char line[EPILOG_LINESIZE];
    //char strokeLine[EPILOG_LINESIZE];
    
    
    /* hack single-page output */
    if (epilog->page_count)
      return 0;
    
    if(type == gx_path_type_clip)
        dlprintf("Clip path epilog_moveto\n");
    
    /* skip non-drawing paths for now */
    if (!(type & gx_path_type_fill) && !(type & gx_path_type_stroke))
    {
        dlprintf("Skipping a path? epilog_moveto\n");
        return 0;
    }
    // Hack to make the tab levels work
    epilog_write(epilog, "");
    if_debug4m('_', epilog->memory, "epilog_moveto(%lf,%lf,%lf,%lf) ", x0, y0, x, y);
    epilog_print_path_type(epilog, type);
    if_debug0m('_', epilog->memory, "\n");
    
    // Make path tags
//    tabMaker(epilog);
//    strokeString(epilog, strokeLine,type);
//    sprintf(line, "%s<path %s>\n", epilog->tabs, strokeLine);
//    epilog_write(epilog, line);
//    epilog->tabLevel++;
//    epilog->pathLevel++;
    
    // Write move
    tabMaker(epilog);
    sprintf(line, "%s<Move>%lf,%lf</Move>\n", epilog->tabs, x, y);
    epilog_write(epilog, line);
    
    // Close path tag
//    epilog->tabLevel--;
//    epilog->pathLevel--;
//    tabMaker(epilog);
//    sprintf(line, "%s</path>\n", epilog->tabs);
//    epilog_write(epilog, line);
    
    return 0;
}

static int
epilog_lineto(gx_device_vector *vdev, floatp x0, floatp y0,
           floatp x, floatp y, gx_path_type_t type)
{
    gx_device_epilog *epilog = (gx_device_epilog *)vdev;
    char line[EPILOG_LINESIZE];
    char strokeLine[EPILOG_LINESIZE];

    /* hack single-page output */
    if (epilog->page_count)
      return 0;
    
    if(type == gx_path_type_clip)
        dlprintf("Clip path epilog_lineto\n");

    /* skip non-drawing paths for now */
    if (!(type & gx_path_type_fill) && !(type & gx_path_type_stroke))
    {
        dlprintf("Skipping a path? epilog_lineto\n");
        return 0;
    }
    // Hack to make the tab levels work
    epilog_write(epilog, "");
    if_debug4m('_', epilog->memory, "epilog_lineto(%lf,%lf,%lf,%lf) ", x0,y0, x,y);
    epilog_print_path_type(epilog, type);
    if_debug0m('_', epilog->memory, "\n");
    tabMaker(epilog);
    strokeString(epilog, strokeLine,type);
    sprintf(line, "%s<Line>%lf,%lf</Line>\n", epilog->tabs, x, y);
    epilog_write(epilog, line);

    return 0;
}

static int
epilog_curveto(gx_device_vector *vdev, floatp x0, floatp y0,
            floatp x1, floatp y1, floatp x2, floatp y2,
            floatp x3, floatp y3, gx_path_type_t type)
{
    gx_device_epilog *epilog = (gx_device_epilog *)vdev;
    char line[EPILOG_LINESIZE];
    char strokeLine[EPILOG_LINESIZE];

    /* hack single-page output */
    if (epilog->page_count)
      return 0;
    
    if(type == gx_path_type_clip)
        dlprintf("Clip path epilog_curveto\n");

    /* skip non-drawing paths for now */
    if (!(type & gx_path_type_fill) && !(type & gx_path_type_stroke))
    {
        dlprintf("Skipping a path? epilog_curveto\n");
        return 0;
    }
    // Hack to make the tab levels work
    epilog_write(epilog, "");
    /* Debug printing */
    if_debug8m('_', epilog->memory, "epilog_curveto(%lf,%lf, %lf,%lf, %lf,%lf, %lf,%lf) ",
        x0,y0, x1,y1, x2,y2, x3,y3);
    epilog_print_path_type(epilog, type);
    if_debug0m('_', epilog->memory, "\n");
    tabMaker(epilog);
    strokeString(epilog, strokeLine,type);
    sprintf(line, "%s<Curve>%lf,%lf %lf,%lf %lf,%lf</Curve>\n", epilog->tabs, x1,y1, x2,y2, x3,y3);
    epilog_write(epilog, line);

    return 0;
}

static int
epilog_closepath(gx_device_vector *vdev, floatp x, floatp y,
              floatp x_start, floatp y_start, gx_path_type_t type)
{
    gx_device_epilog *epilog = (gx_device_epilog *)vdev;
    char line[EPILOG_LINESIZE];
    char strokeLine[EPILOG_LINESIZE];

    /* hack single-page output */
    if (epilog->page_count)
      return 0;

    /* skip non-drawing paths for now */
    if (!(type & gx_path_type_fill) && !(type & gx_path_type_stroke))
    {
        dlprintf("Skipping a path? epilog_closepath\n");
        return 0;
    }

    if_debug0m('_', epilog->memory, "epilog_closepath ");
    epilog_print_path_type(epilog, type);
    if_debug0m('_', epilog->memory, "\n");

    tabMaker(epilog);
    strokeString(epilog, strokeLine, type);
    sprintf(line, "%s<ClosePath></ClosePath>\n", epilog->tabs);
    epilog_write(epilog, line);
//    sprintf(line, "%s<Line>%lf,%lf</Line><!-- %lf,%lf -->\n", epilog->tabs, x_start, y_start,x,y);
//    epilog_write(epilog, line);
    
    //Write an end path tag
//    epilog->tabLevel--;
//    tabMaker(epilog);
//    sprintf(line, "%s</path>\n", epilog->tabs);
//    epilog_write(epilog, line);

    return 0;
}

static int
epilog_endpath(gx_device_vector *vdev, gx_path_type_t type)
{
    gx_device_epilog *epilog = (gx_device_epilog *)vdev;
    char line[EPILOG_LINESIZE];
    /* hack single-page output */
    if (epilog->page_count)
      return 0;

    /* skip non-drawing paths for now */
    if (!(type & gx_path_type_fill) && !(type & gx_path_type_stroke))
      return 0;

    if_debug0m('_', epilog->memory, "epilog_endpath ");
    epilog_print_path_type(epilog, type);
    if_debug0m('_', epilog->memory, "\n");
    //Close path
    tabMaker(epilog);
    sprintf(line, "%s<ClosePath></ClosePath>\n", epilog->tabs);
    //epilog_write(epilog, line);
    //End path
    epilog->tabLevel--;
    epilog->pathLevel--;
    tabMaker(epilog);
    sprintf(line, "%s</path>\n", epilog->tabs);
    epilog_write(epilog, line);

//    /* close the path data attribute */
//    epilog_write(epilog, "'");
//
//    /* override the inherited stroke attribute if we're not stroking */
//    if (!(type & gx_path_type_stroke) && (epilog->strokecolor != gx_no_color_index))
//      epilog_write(epilog, " stroke='none'");
//
//    /* override the inherited fill attribute if we're not filling */
//    if (!(type & gx_path_type_fill) && (epilog->fillcolor != gx_no_color_index))
//      epilog_write(epilog, " fill='none'");
//
//    epilog_write(epilog, "/>\n");

    return 0;
}

/* ---------------- High-level driver procedures ---------------- */

static int
epilog_fill_path(gx_device * dev, const gs_imager_state * pis,
                gx_path * ppath, const gx_fill_params * params,
                const gx_drawing_color * pdcolor,
                const gx_clip_path * pcpath)
{
    char line[EPILOG_LINESIZE];
    gx_device_epilog *epilog = (gx_device_epilog *)dev;
    epilog_write(epilog, "");
    // Write the path properties
    dmputs(dev->memory,"fill_path({\n");
    if(epilog_valid_color(epilog, "", pdcolor))
    {
        epilog_write_tabbed(epilog, "<fill_path ");
        epilog_drawing_color(epilog,"}, ", pdcolor);
        dmprintf4(dev->memory,", rule=%d, adjust=(%g,%g), flatness=%g",
                  params->rule, fixed2float(params->adjust.x),
                  fixed2float(params->adjust.y), params->flatness);
        sprintf(line, " rule='%s'", params->rule > 0 ? "even-odd" : "winding");
        epilog_write(epilog, line);
        epilog_write(epilog, ">\n");
        increaseIndent(epilog);
        
        // Write the path
        epilog_write_tabbed(epilog, "<path>\n");
        increaseIndent(epilog);
        epilog_path(epilog, ppath);
        decreaseIndent(epilog);
        epilog_write_tabbed(epilog, "</path>\n");
        
        // Write the clip path
        epilog_write_tabbed(epilog, "<clippath>\n");
        increaseIndent(epilog);
        epilog_clip(epilog, pcpath);
        decreaseIndent(epilog);
        epilog_write_tabbed(epilog, "</clippath>\n");
        
        // End the path
        decreaseIndent(epilog);
        epilog_write_tabbed(epilog, "</fill_path>\n");
        /****** pis ******/
        dmputs(dev->memory,")\n");
        return 0;
    }
    else {
        int code;
        // Initiallize the PNG stuff
        gs_fixed_rect r;
        char *encodedPNG;
        size_t encodedPNGLength;
        if(!gx_path_is_void(ppath))
        {
            gx_path_bbox(ppath, &r);
            ppath->bbox = r;
            epilog->offsetPoint = r.p;
            dmprintf2(dev->memory,"%d,%d\n",fixed2int_pixround(r.q.x-r.p.x),fixed2int_pixround(r.q.y-r.p.y));
            dmprintf2(dev->memory,"%d,%d - Perfect\n",fixed2int_pixround_perfect(r.q.x-r.p.x),fixed2int_pixround_perfect(r.q.y-r.p.y));
            rawPNG = 0;
            rawPNGLength = 0;
            epilog_make_path_image(epilog, ppath, pcpath);
            epilog->row_pointers = makePNG(fixed2int_pixround(r.q.y-r.p.y)+PNG_PADDING, fixed2int_pixround(r.q.x-r.p.x)+PNG_PADDING, 4);
            epilog->padding = PNG_PADDING/2;
            
        }
        code = gx_default_fill_path(dev,pis,ppath,params,pdcolor,pcpath);
        // Finish the PNG stuff
        /* write the rest of the file */
        if(!gx_path_is_void(ppath))
        {
            dmprintf2(dev->memory,"%d,%d\n",fixed2int_pixround(r.q.x-r.p.x),fixed2int_pixround(r.q.y-r.p.y));
            png_write_image(epilog->png_struct, epilog->row_pointers);
            png_write_end(epilog->png_struct,epilog->png_info);
        
    #if PNG_LIBPNG_VER_MINOR >= 5
    #else
            /* if you alloced the palette, free it here */
            gs_free_object(mem, epilog->palettep, "png palette");
    #endif
            // Free png data
            freePNG(epilog->row_pointers, fixed2int_pixround(r.q.y-r.p.y), fixed2int_pixround(r.q.x-r.p.x));
            
            // Encode data
            encodedPNG = base64_encode(rawPNG, rawPNGLength, &encodedPNGLength);
            
            // Write data to file
            epilog_write_tabbed(epilog, "<pathImage");
            sprintf(line, " x='%f' y='%f' encoding='png/base64'>\n",fixed2float(r.p.x)-PNG_PADDING/2,fixed2float(r.p.y)-PNG_PADDING/2);
            epilog_write(epilog, line);
            increaseIndent(epilog);
            epilog_write_base64(epilog, encodedPNG, encodedPNGLength);
            decreaseIndent(epilog);
            epilog_write_tabbed(epilog, "</pathImage>\n");
            
            // Free data and encoded data
            free(rawPNG);
            free(encodedPNG);
            
        done:
            /* free the structures */
            png_destroy_write_struct(&epilog->png_struct, &epilog->png_info);
        }
        return code;
    }
}

static int
epilog_stroke_path(gx_device * dev, const gs_imager_state * pis,
                  gx_path * ppath, const gx_stroke_params * params,
                  const gx_drawing_color * pdcolor,
                  const gx_clip_path * pcpath)
{
//    char line[EPILOG_LINESIZE];
//    gx_device_epilog *epilog = (gx_device_epilog *)dev;
//    dmputs(dev->memory,"stroke_path({\n");
//    epilog_path(epilog,ppath);
//    epilog_drawing_color(epilog,"}, ", pdcolor);
//    dmprintf1(dev->memory,", flatness=%g", params->flatness);
//    epilog_clip(epilog, pcpath);
//    /****** pis ******/
//    dmputs(dev->memory,")\n");
//    return 0;
    
    char line[EPILOG_LINESIZE];
    gx_device_epilog *epilog = (gx_device_epilog *)dev;
    epilog_write(epilog, "");
    // Write the path properties
    dmputs(dev->memory,"stroke_path({\n");    
    epilog_write_tabbed(epilog, "<stroke_path ");
    epilog_drawing_color(epilog,"}, ", pdcolor);
    dmprintf1(dev->memory,", flatness=%g", params->flatness);
    // Stroke width
    sprintf(line, " width='%f'",pis->line_params.half_width/36.0*epilog->HWResolution[0]);
    epilog_write(epilog, line);
    sprintf(line, " cap='%d' join='%d' miter='%f'",pis->line_params.start_cap,pis->line_params.join,pis->line_params.miter_limit);
    epilog_write(epilog, line);
    
    epilog_write(epilog, ">\n");
    increaseIndent(epilog);
    
    // Write the path
    epilog_write_tabbed(epilog, "<path>\n");
    increaseIndent(epilog);
    epilog_path(epilog, ppath);
    decreaseIndent(epilog);
    epilog_write_tabbed(epilog, "</path>\n");
    
    // Write the clip path
    epilog_write_tabbed(epilog, "<clippath>\n");
    increaseIndent(epilog);
    epilog_clip(epilog, pcpath);
    decreaseIndent(epilog);
    epilog_write_tabbed(epilog, "</clippath>\n");
    
    // End the path
    decreaseIndent(epilog);
    epilog_write_tabbed(epilog, "</stroke_path>\n");
    /****** pis ******/
    dmputs(dev->memory,")\n");
    return 0;
}

typedef struct epilog_image_enum_s {
    gx_image_enum_common;
    int rows_left;
} epilog_image_enum_t;
gs_private_st_suffix_add0(st_epilog_image_enum, epilog_image_enum_t,
                          "epilog_image_enum_t", epilog_image_enum_enum_ptrs,
                          epilog_image_enum_reloc_ptrs,
                          st_gx_image_enum_common);
static int
epilog_plane_data(gx_image_enum_common_t * info,
                 const gx_image_plane_t * planes, int height,
                 int *rows_used)
{
    epilog_image_enum_t *pie = (epilog_image_enum_t *)info;
    gx_device_epilog *epilog = (gx_device_epilog *)info->dev;
    //char *output;
    //size_t encodeLength;
    int i,j,k,raster,ind,bytes;
    double black;
    byte *row;
    unsigned char rgb[3];
    unsigned char cmyk[4];
    // Allocate memory
    raster = pie->num_planes*pie->plane_widths[0]*pie->plane_depths[0]/8;
    row = gs_alloc_bytes(epilog->memory, raster, "png raster buffer");
    // Clear memory
    memset(row,0xff,raster);
    //dmprintf1(info->memory,"allocate %d bytes\n", raster);
//    dmprintf1(info->memory,"Image index = %d\n", pie->image_type->index);
    
    
//    dmprintf1(info->memory,"image_plane_data(height=%d", height);
    for(j = 0; j < height; j++)
    {
        for (i = 0; i < pie->plane_widths[0]; i++) {
            for (k = 0; k < pie->num_planes; ++k) {
                if (planes[k].data && pie->num_planes < 4)
                {
                    bytes = pie->plane_depths[k]/8;
                    ind = i*pie->num_planes*bytes+k;
                    memcpy(&row[ind],&planes[k].data[i*bytes],bytes);
                }
                else if(pie->num_planes == 4)
                {
                    //
                    cmyk[k] = planes[k].data[i];
                }
                else
                {
                    dmprintf(info->memory," no data or too many planes");
                    memset(&row[i+k],0xff,pie->plane_depths[k]/8);
                }
            }
            if(pie->num_planes == 4)
            {
                black = (1.0-cmyk[3]/255.0);
                rgb[2] = 255*(1.0-cmyk[0]/255.0)*black;
                rgb[1] = 255*(1.0-cmyk[1]/255.0)*black;
                rgb[0] = 255*(1.0-cmyk[2]/255.0)*black;
                memcpy(&row[i*3],rgb,3);
            }
        }
        png_write_rows(epilog->png_struct, &row, 1);
    }
//    for (i = 0; i < pie->num_planes; ++i) {
//        if (planes[i].data)
//        {
//            dmprintf4(info->memory,", {depth=%d, width=%d, dx=%d, raster=%u}",
//                      pie->plane_depths[i], pie->plane_widths[i],
//                      planes[i].data_x, planes[i].raster);
////            output = base64_encode(planes[i].data, planes[i].raster, &encodeLength);
////            epilog_write_base64(epilog, output, encodeLength);
////            epilog_write_bytes(epilog, planes[i].data, planes[i].raster*height);
//            
//        }
//        else
//            dmputs(info->memory,", -");
//    }
//    dmputs(info->memory,")\n");
    *rows_used = height;
    gs_free_object(epilog->memory, row, "png raster buffer");
    return (pie->rows_left -= height) <= 0;
}
static int
epilog_end_image(gx_image_enum_common_t * info, bool draw_last)
{
    //epilog_image_enum_t *pie = (epilog_image_enum_t *)info;
    gx_device_epilog *epilog = (gx_device_epilog *)info->dev;
    epilog_write_tabbed(epilog, "<imfile>\n");
    increaseIndent(epilog);
    epilog_write_tabbed(epilog, epilog->fileName);
    epilog_write(epilog,"\n");
    decreaseIndent(epilog);
    epilog_write_tabbed(epilog, "</imfile>\n");
    decreaseIndent(epilog);
    epilog_write_tabbed(epilog, "</image>\n");
    // Finish the PNG stuff
    /* write the rest of the file */
    png_write_end(epilog->png_struct,epilog->png_info);
    
#if PNG_LIBPNG_VER_MINOR >= 5
#else
    /* if you alloced the palette, free it here */
    gs_free_object(mem, epilog->palettep, "png palette");
#endif
    
    fclose(epilog->fp);
    
done:
    /* free the structures */
    png_destroy_write_struct(&epilog->png_struct, &epilog->png_info);
    
    gx_image_free_enum(&info);
    return 0;
}
static const gx_image_enum_procs_t epilog_image_enum_procs = {
    epilog_plane_data, epilog_end_image
};

// TODO: Ensure the correct flow under fault conditions
static int
epilog_begin_typed_image(gx_device * dev, const gs_imager_state * pis,
                        const gs_matrix * pmat,
                        const gs_image_common_t * pim,
                        const gs_int_rect * prect,
                        const gx_drawing_color * pdcolor,
                        const gx_clip_path * pcpath,
                        gs_memory_t * memory,
                        gx_image_enum_common_t ** pinfo)
{
    epilog_image_enum_t *pie;
    const gs_pixel_image_t *ppi = (const gs_pixel_image_t *)pim;
    gx_device_epilog *epilog = (gx_device_epilog *)dev;
    char line[EPILOG_LINESIZE];
    int dst_bpc, src_bpc;
    char software_key[80];
    char software_text[256];
    png_text text_png;
    int ncomp;
    int code;			/* return code */
    png_struct *png_ptr;
    png_info *info_ptr;
    png_byte bit_depth = 0;
    png_byte color_type = 0;
    png_uint_32 x_pixels_per_unit;
    png_uint_32 y_pixels_per_unit;
    png_byte phys_unit_type;
    png_color_16 background;
    png_uint_32 width, height;
#if PNG_LIBPNG_VER_MINOR >= 5
    png_color palette[256];
#endif
    png_color *palettep;
    png_uint_16 num_palette;
    png_uint_32 valid = 0;
    int factor = 1;
    //int mfs = 1;
    bool errdiff = 0;
    int depth;
    bool invert = false, endian_swap = false, bg_needed = false;
    
    //Initialize the text output system
    epilog_write(epilog, "");
    
    // Do the printing
    switch (pim->type->index) {
        case 1:
            if (((const gs_image1_t *)ppi)->ImageMask) {
                ncomp = 1;
                break;
            }
            /* falls through */
        case 4:
            ncomp = gs_color_space_num_components(ppi->ColorSpace);
            break;
        case 3:
            ncomp = gs_color_space_num_components(ppi->ColorSpace) + 1;
            break;
        case 2:			/* no data */
            dmputs(dev->memory, ")\n");
            return 1;
        default:
            goto dflt;
    }
    pie = gs_alloc_struct(memory, epilog_image_enum_t, &st_epilog_image_enum,
                          "epilog_begin_typed_image");
    if (pie == 0)
        goto dflt;
    
    if (gx_image_enum_common_init((gx_image_enum_common_t *)pie,
                                  (const gs_data_image_t *)pim,
                                  &epilog_image_enum_procs, dev, ncomp,
                                  ppi->format) < 0
        )
        goto dflt;
//    dmprintf5(dev->memory,"\n    Width=%d, Height=%d, BPC=%d, num_components=%d num_planes=%d)\n",
//              ppi->Width, ppi->Height, ppi->BitsPerComponent, ncomp, pie->num_planes);
    pie->memory = memory;
    pie->rows_left = ppi->Height;
    *pinfo = (gx_image_enum_common_t *)pie;
    
    if(pie->num_planes < 4)
        depth = ppi->BitsPerComponent*ncomp;
    else if(pie->num_planes == 4)
    {
        depth = 24;
    }
    else{
        gs_note_error(gs_error_Remap_Color);
    }
    // Write out the begin tag
    epilog_write_tabbed(epilog, "<image ");
    sprintf(line, "xx='%f' xy='%f' yx='%f' yy='%f' tx='%f' ty='%f' ",pis->ctm.xx, pis->ctm.xy,
            pis->ctm.yx, pis->ctm.yy,
            pis->ctm.tx, pis->ctm.ty);
    epilog_write(epilog,line);
    sprintf(line, "width='%d' height='%d' BPC='%d' num_components='%d' num_planes='%d' ",
            ppi->Width, ppi->Height, ppi->BitsPerComponent, ncomp, pie->num_planes);
    epilog_write(epilog, line);
    epilog_write(epilog, ">\n");
    increaseIndent(epilog);
    // Write the clip path
    epilog_write_tabbed(epilog, "<clippath>\n");
    increaseIndent(epilog);
    if(pcpath->path_list) {
        gx_cpath_path_list *path_list = pcpath->path_list;
        do {
            dmprintf1(epilog->memory, "Rule: %d\n", path_list->rule);
            epilog_path(epilog, &path_list->path);
        } while ((path_list = path_list->next));
    }
    epilog_clip(epilog, pcpath);
    decreaseIndent(epilog);
    epilog_write_tabbed(epilog, "</clippath>\n");
    
    
    // Open up the file to write
    sprintf(epilog->fileName, "image%d.png",epilog->numImages++);
    epilog->fp = fopen(epilog->fileName, "wb");
    dmprintf1(epilog->memory, "Saving %s\n", epilog->fileName);
    // Prepare the PNG structures
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    info_ptr = png_create_info_struct(png_ptr);
    epilog->png_struct = png_ptr;
    epilog->png_info = info_ptr;
    
    if (png_ptr == 0 || info_ptr == 0) {
        code = gs_note_error(gs_error_VMerror);
        goto done;
    }
    /* set error handling */
#if PNG_LIBPNG_VER_MINOR >= 5
    code = setjmp(png_jmpbuf(png_ptr));
#else
    code = setjmp(png_ptr->jmpbuf);
#endif
    if (code) {
        /* If we get here, we had a problem reading the file */
        code = gs_note_error(gs_error_VMerror);
        goto done;
    }
    code = 0;			/* for normal path */
    /* set up the output control */
    png_init_io(png_ptr, epilog->fp);
    /* set the file information here */
    /* resolution is in pixels per meter vs. dpi */
    x_pixels_per_unit =
    (png_uint_32) (dev->HWResolution[0] * (100.0 / 2.54) / factor + 0.5);
    y_pixels_per_unit =
    (png_uint_32) (dev->HWResolution[1] * (100.0 / 2.54) / factor + 0.5);
    
    phys_unit_type = PNG_RESOLUTION_METER;
    valid |= PNG_INFO_pHYs;
    
    switch (depth) {
        case 32:
            bit_depth = 8;
            color_type = PNG_COLOR_TYPE_RGB_ALPHA;
            invert = true;
            
        {
            background.index = 0;
            background.red = 0xff;
            background.green = 0xff;
            background.blue = 0xff;
            background.gray = 0;
            bg_needed = true;
        }
            break;
        case 48:
            bit_depth = 16;
            color_type = PNG_COLOR_TYPE_RGB;
#if defined(ARCH_IS_BIG_ENDIAN) && (!ARCH_IS_BIG_ENDIAN)
            endian_swap = true;
#endif
            break;
        case 24:
            bit_depth = 8;
            color_type = PNG_COLOR_TYPE_RGB;
            errdiff = 1;
            break;
        case 8:
            bit_depth = 8;
            // TODO dev needs to change to the picture
            if (ppi->type->index == gs_color_space_index_Indexed) {
                color_type = PNG_COLOR_TYPE_PALETTE;
                errdiff = 0;
                dmprintf(dev->memory, "The picture is indexed :(");
            } else {
                color_type = PNG_COLOR_TYPE_GRAY;
                errdiff = 1;
            }
            break;
        case 4:
            bit_depth = 4;
            color_type = PNG_COLOR_TYPE_PALETTE;
            break;
        case 1:
            bit_depth = 1;
            color_type = PNG_COLOR_TYPE_GRAY;
            break;
    }
    
    /* set the palette if there is one */
    if (color_type == PNG_COLOR_TYPE_PALETTE) {
        int i;
        int num_colors = 1 << depth;
        gx_color_value rgb[3];
        
#if PNG_LIBPNG_VER_MINOR >= 5
        palettep = palette;
#else
        palettep =
        (void *)gs_alloc_bytes(mem, 256 * sizeof(png_color),
                               "png palette");
        if (palettep == 0) {
            code = gs_note_error(gs_error_VMerror);
            goto done;
        }
#endif
        num_palette = num_colors;
        valid |= PNG_INFO_PLTE;
        for (i = 0; i < num_colors; i++) {
            (*dev_proc(dev, map_color_rgb)) ((gx_device *) dev, (gx_color_index) i, rgb);
            palettep[i].red = gx_color_value_to_byte(rgb[0]);
            palettep[i].green = gx_color_value_to_byte(rgb[1]);
            palettep[i].blue = gx_color_value_to_byte(rgb[2]);
            dmprintf3(dev->memory,"r=%d g=%d b=%d\n",
                      palettep[i].red,palettep[i].green,palettep[i].blue);
        }
    }
    else {
        palettep = NULL;
        num_palette = 0;
    }
    /* add comment */
    strncpy(software_key, "Software", sizeof(software_key));
    text_png.compression = -1;	/* uncompressed */
    text_png.key = software_key;
    text_png.text = "Epilog+LibPNG";
    text_png.text_length = strlen(software_text);
    
    dst_bpc = bit_depth;
    src_bpc = dst_bpc;
    if (errdiff)
        src_bpc = 8;
    else
        factor = 1;
    
    
    width = ppi->Width;
    height = ppi->Height;
    
#if PNG_LIBPNG_VER_MINOR >= 5
    png_set_pHYs(png_ptr, info_ptr,
                 x_pixels_per_unit, y_pixels_per_unit, phys_unit_type);
    
    png_set_IHDR(png_ptr, info_ptr,
                 width, height, bit_depth,
                 color_type, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_DEFAULT);
    if (palettep)
        png_set_PLTE(png_ptr, info_ptr, palettep, num_palette);
    
    png_set_text(png_ptr, info_ptr, &text_png, 1);
#else
    info_ptr->bit_depth = bit_depth;
    info_ptr->color_type = color_type;
    info_ptr->width = width;
    info_ptr->height = height;
    info_ptr->x_pixels_per_unit = x_pixels_per_unit;
    info_ptr->y_pixels_per_unit = y_pixels_per_unit;
    info_ptr->phys_unit_type = phys_unit_type;
    info_ptr->palette = palettep;
    info_ptr->num_palette = num_palette;
    info_ptr->valid |= valid;
    info_ptr->text = &text_png;
    info_ptr->num_text = 1;
    /* Set up the ICC information */
    if (ppi->cmm_icc_profile_data != NULL) {
        cmm_profile_t *icc_profile = ppi->cmm_icc_profile_data;
        /* PNG can only be RGB or gray.  No CIELAB :(  */
        if (icc_profile->data_cs == gsRGB || icc_profile->data_cs == gsGRAY) {
            if (icc_profile->num_comps == pdev->color_info.num_components) {
                info_ptr->iccp_name = icc_profile->name;
                info_ptr->iccp_profile = icc_profile->buffer;
                info_ptr->iccp_proflen = icc_profile->buffer_size;
                info_ptr->valid |= PNG_INFO_iCCP;
            }
        }
    }
#endif
    if (invert) {
        if (depth == 32)
            png_set_invert_alpha(png_ptr);
        else
            png_set_invert_mono(png_ptr);
    }
    if (bg_needed) {
        png_set_bKGD(png_ptr, info_ptr, &background);
    }
#if defined(ARCH_IS_BIG_ENDIAN) && (!ARCH_IS_BIG_ENDIAN)
    if (endian_swap) {
        png_set_swap(png_ptr);
    }
#endif
    
    /* write the file information */
    png_write_info(png_ptr, info_ptr);
    
#if PNG_LIBPNG_VER_MINOR >= 5
#else
    /* don't write the comments twice */
    info_ptr->num_text = 0;
    info_ptr->text = NULL;
    // save the pallet so it can be cleaned up later
    epilog->palettep = palettep;
#endif
    
done:
    return 0;
dflt:
    dmputs(dev->memory,") DEFAULTED\n");
    return gx_default_begin_typed_image(dev, pis, pmat, pim, prect, pdcolor,
                                        pcpath, memory, pinfo);
}

static int
epilog_make_path_image(gx_device_epilog *epilog, gx_path * ppath, const gx_clip_path * pcpath)
{
    char line[EPILOG_LINESIZE];
    int dst_bpc, src_bpc;
    char software_key[80];
    char software_text[256];
    png_text text_png;
    int code;			/* return code */
    png_struct *png_ptr;
    png_info *info_ptr;
    png_byte bit_depth = 0;
    png_byte color_type = 0;
    png_uint_32 x_pixels_per_unit;
    png_uint_32 y_pixels_per_unit;
    png_byte phys_unit_type;
    png_color_16 background;
    png_uint_32 width, height;
    png_color *palettep;
    png_uint_16 num_palette;
    png_uint_32 valid = 0;
    int factor = 1;
    //int mfs = 1;
    bool errdiff = 0;
    int depth;
    bool invert = false, endian_swap = false, bg_needed = false;
    
    
    
//    // Open up the file to write
//    sprintf(epilog->fileName, "path%d.png",epilog->numPaths++);
//    epilog->fp = fopen(epilog->fileName, "wb");
//    dmprintf1(epilog->memory, "Saving %s\n", epilog->fileName);
    // Prepare the PNG structures
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    info_ptr = png_create_info_struct(png_ptr);
    epilog->png_struct = png_ptr;
    epilog->png_info = info_ptr;
    
    // Set the output functions to user_write_data
    png_set_write_fn(png_ptr, NULL , &user_write_data,
                     &user_flush_data);
    
    if (png_ptr == 0 || info_ptr == 0) {
        code = gs_note_error(gs_error_VMerror);
        goto done;
    }
    /* set error handling */
#if PNG_LIBPNG_VER_MINOR >= 5
    code = setjmp(png_jmpbuf(png_ptr));
#else
    code = setjmp(png_ptr->jmpbuf);
#endif
    if (code) {
        /* If we get here, we had a problem reading the file */
        code = gs_note_error(gs_error_VMerror);
        goto done;
    }
    code = 0;			/* for normal path */
//    /* set up the output control */
//    png_init_io(png_ptr, epilog->fp);
    /* set the file information here */
    /* resolution is in pixels per meter vs. dpi */
    x_pixels_per_unit =
    (png_uint_32) (epilog->HWResolution[0] * (100.0 / 2.54) / factor + 0.5);
    y_pixels_per_unit =
    (png_uint_32) (epilog->HWResolution[1] * (100.0 / 2.54) / factor + 0.5);
    
    phys_unit_type = PNG_RESOLUTION_METER;
    valid |= PNG_INFO_pHYs;
    
    
    // Set the bit depth to be 32 with alpha channel
    bit_depth = 8;
    color_type = PNG_COLOR_TYPE_RGB_ALPHA;
    invert = true;
    background.index = 0;
    background.red = 0xff;
    background.green = 0xff;
    background.blue = 0xff;
    background.gray = 0;
    bg_needed = true;
    
    /* set the palette if there is one */
    palettep = NULL;
    num_palette = 0;
    
    /* add comment */
    strncpy(software_key, "Software", sizeof(software_key));
    text_png.compression = -1;	/* uncompressed */
    text_png.key = software_key;
    text_png.text = (char*)"Epilog+LibPNG";
    text_png.text_length = strlen(software_text);
    
    dst_bpc = bit_depth;
    src_bpc = dst_bpc;
    if (errdiff)
        src_bpc = 8;
    else
        factor = 1;
    
    
    width = fixed2int_pixround(ppath->bbox.q.x-ppath->bbox.p.x);
    height = fixed2int_pixround(ppath->bbox.q.y-ppath->bbox.p.y);
//    width = ppi->Width;
//    height = ppi->Height;
    
#if PNG_LIBPNG_VER_MINOR >= 5
    png_set_pHYs(png_ptr, info_ptr,
                 x_pixels_per_unit, y_pixels_per_unit, phys_unit_type);
    
    png_set_IHDR(png_ptr, info_ptr,
                 width, height, bit_depth,
                 color_type, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_DEFAULT);
    if (palettep)
        png_set_PLTE(png_ptr, info_ptr, palettep, num_palette);
    
    png_set_text(png_ptr, info_ptr, &text_png, 1);
#else
    info_ptr->bit_depth = bit_depth;
    info_ptr->color_type = color_type;
    info_ptr->width = width;
    info_ptr->height = height;
    info_ptr->x_pixels_per_unit = x_pixels_per_unit;
    info_ptr->y_pixels_per_unit = y_pixels_per_unit;
    info_ptr->phys_unit_type = phys_unit_type;
    info_ptr->palette = palettep;
    info_ptr->num_palette = num_palette;
    info_ptr->valid |= valid;
    info_ptr->text = &text_png;
    info_ptr->num_text = 1;
    /* Set up the ICC information */
    if (ppi->cmm_icc_profile_data != NULL) {
        cmm_profile_t *icc_profile = ppi->cmm_icc_profile_data;
        /* PNG can only be RGB or gray.  No CIELAB :(  */
        if (icc_profile->data_cs == gsRGB || icc_profile->data_cs == gsGRAY) {
            if (icc_profile->num_comps == pdev->color_info.num_components) {
                info_ptr->iccp_name = icc_profile->name;
                info_ptr->iccp_profile = icc_profile->buffer;
                info_ptr->iccp_proflen = icc_profile->buffer_size;
                info_ptr->valid |= PNG_INFO_iCCP;
            }
        }
    }
#endif
    if (invert) {
        if (depth == 32)
            png_set_invert_alpha(png_ptr);
        else
            png_set_invert_mono(png_ptr);
    }
    if (bg_needed) {
        png_set_bKGD(png_ptr, info_ptr, &background);
    }
#if defined(ARCH_IS_BIG_ENDIAN) && (!ARCH_IS_BIG_ENDIAN)
    if (endian_swap) {
        png_set_swap(png_ptr);
    }
#endif
    
    /* write the file information */
    png_write_info(png_ptr, info_ptr);
    
#if PNG_LIBPNG_VER_MINOR >= 5
#else
    /* don't write the comments twice */
    info_ptr->num_text = 0;
    info_ptr->text = NULL;
    // save the pallet so it can be cleaned up later
    epilog->palettep = palettep;
#endif
done:
    return 0;
}

static int
epilog_make_png_image(gx_device_epilog *epilog, int w, int h)
{
    char line[EPILOG_LINESIZE];
    int dst_bpc, src_bpc;
    char software_key[80];
    char software_text[256];
    png_text text_png;
    int code;			/* return code */
    png_struct *png_ptr;
    png_info *info_ptr;
    png_byte bit_depth = 0;
    png_byte color_type = 0;
    png_uint_32 x_pixels_per_unit;
    png_uint_32 y_pixels_per_unit;
    png_byte phys_unit_type;
    png_color_16 background;
    png_uint_32 width, height;
    png_color *palettep;
    png_uint_16 num_palette;
    png_uint_32 valid = 0;
    int factor = 1;
    //int mfs = 1;
    bool errdiff = 0;
    int depth;
    bool invert = false, endian_swap = false, bg_needed = false;
    
    
    
    //    // Open up the file to write
    //    sprintf(epilog->fileName, "path%d.png",epilog->numPaths++);
    //    epilog->fp = fopen(epilog->fileName, "wb");
    //    dmprintf1(epilog->memory, "Saving %s\n", epilog->fileName);
    // Prepare the PNG structures
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    info_ptr = png_create_info_struct(png_ptr);
    epilog->png_struct = png_ptr;
    epilog->png_info = info_ptr;
    
    // Set the output functions to user_write_data
    png_set_write_fn(png_ptr, NULL , &user_write_data,
                     &user_flush_data);
    
    if (png_ptr == 0 || info_ptr == 0) {
        code = gs_note_error(gs_error_VMerror);
        goto done;
    }
    /* set error handling */
#if PNG_LIBPNG_VER_MINOR >= 5
    code = setjmp(png_jmpbuf(png_ptr));
#else
    code = setjmp(png_ptr->jmpbuf);
#endif
    if (code) {
        /* If we get here, we had a problem reading the file */
        code = gs_note_error(gs_error_VMerror);
        goto done;
    }
    code = 0;			/* for normal path */
    //    /* set up the output control */
    //    png_init_io(png_ptr, epilog->fp);
    /* set the file information here */
    /* resolution is in pixels per meter vs. dpi */
    x_pixels_per_unit =
    (png_uint_32) (epilog->HWResolution[0] * (100.0 / 2.54) / factor + 0.5);
    y_pixels_per_unit =
    (png_uint_32) (epilog->HWResolution[1] * (100.0 / 2.54) / factor + 0.5);
    
    phys_unit_type = PNG_RESOLUTION_METER;
    valid |= PNG_INFO_pHYs;
    
    
    // Set the bit depth to be 32 with alpha channel
    bit_depth = 8;
    color_type = PNG_COLOR_TYPE_RGB_ALPHA;
    invert = true;
    background.index = 0;
    background.red = 0xff;
    background.green = 0xff;
    background.blue = 0xff;
    background.gray = 0;
    bg_needed = true;
    
    /* set the palette if there is one */
    palettep = NULL;
    num_palette = 0;
    
    /* add comment */
    strncpy(software_key, "Software", sizeof(software_key));
    text_png.compression = -1;	/* uncompressed */
    text_png.key = software_key;
    text_png.text = (char*)"Epilog+LibPNG";
    text_png.text_length = strlen(software_text);
    
    dst_bpc = bit_depth;
    src_bpc = dst_bpc;
    if (errdiff)
        src_bpc = 8;
    else
        factor = 1;
    
    
    width = w;
    height = h;
    //    width = ppi->Width;
    //    height = ppi->Height;
    
#if PNG_LIBPNG_VER_MINOR >= 5
    png_set_pHYs(png_ptr, info_ptr,
                 x_pixels_per_unit, y_pixels_per_unit, phys_unit_type);
    
    png_set_IHDR(png_ptr, info_ptr,
                 width, height, bit_depth,
                 color_type, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_DEFAULT);
    if (palettep)
        png_set_PLTE(png_ptr, info_ptr, palettep, num_palette);
    
    png_set_text(png_ptr, info_ptr, &text_png, 1);
#else
    info_ptr->bit_depth = bit_depth;
    info_ptr->color_type = color_type;
    info_ptr->width = width;
    info_ptr->height = height;
    info_ptr->x_pixels_per_unit = x_pixels_per_unit;
    info_ptr->y_pixels_per_unit = y_pixels_per_unit;
    info_ptr->phys_unit_type = phys_unit_type;
    info_ptr->palette = palettep;
    info_ptr->num_palette = num_palette;
    info_ptr->valid |= valid;
    info_ptr->text = &text_png;
    info_ptr->num_text = 1;
    /* Set up the ICC information */
    if (ppi->cmm_icc_profile_data != NULL) {
        cmm_profile_t *icc_profile = ppi->cmm_icc_profile_data;
        /* PNG can only be RGB or gray.  No CIELAB :(  */
        if (icc_profile->data_cs == gsRGB || icc_profile->data_cs == gsGRAY) {
            if (icc_profile->num_comps == pdev->color_info.num_components) {
                info_ptr->iccp_name = icc_profile->name;
                info_ptr->iccp_profile = icc_profile->buffer;
                info_ptr->iccp_proflen = icc_profile->buffer_size;
                info_ptr->valid |= PNG_INFO_iCCP;
            }
        }
    }
#endif
    if (invert) {
        if (depth == 32)
            png_set_invert_alpha(png_ptr);
        else
            png_set_invert_mono(png_ptr);
    }
    if (bg_needed) {
        png_set_bKGD(png_ptr, info_ptr, &background);
    }
#if defined(ARCH_IS_BIG_ENDIAN) && (!ARCH_IS_BIG_ENDIAN)
    if (endian_swap) {
        png_set_swap(png_ptr);
    }
#endif
    
    /* write the file information */
    png_write_info(png_ptr, info_ptr);
    
#if PNG_LIBPNG_VER_MINOR >= 5
#else
    /* don't write the comments twice */
    info_ptr->num_text = 0;
    info_ptr->text = NULL;
    // save the pallet so it can be cleaned up later
    epilog->palettep = palettep;
#endif
done:
    return 0;
}

static int
epilog_text_process(gs_text_enum_t *pte)
{
    dmputs(pte->dev->memory,"text_process\n");
    epilog_path((gx_device_epilog*)pte->dev, pte->path);
    return 0;
}
static const gs_text_enum_procs_t epilog_text_procs = {
    NULL, epilog_text_process, NULL, NULL, NULL, NULL,
    gx_default_text_release
};


static int
epilog_text_begin(gx_device * dev, gs_imager_state * pis,
                 const gs_text_params_t * text, gs_font * font,
                 gx_path * path, const gx_device_color * pdcolor,
                 const gx_clip_path * pcpath, gs_memory_t * memory,
                 gs_text_enum_t ** ppenum)
{
    static const char *const tags[sizeof(text->operation) * 8] = {
        "FROM_STRING", "FROM_BYTES", "FROM_CHARS", "FROM_GLYPHS",
        "FROM_SINGLE_CHAR", "FROM_SINGLE_GLYPH",
        "ADD_TO_ALL_WIDTHS", "ADD_TO_SPACE_WIDTH",
        "REPLACE_WIDTHS", "DO_NONE", "DO_DRAW", "DO_CHARWIDTH",
        "DO_FALSE_CHARPATH", "DO_TRUE_CHARPATH",
        "DO_FALSE_CHARBOXPATH", "DO_TRUE_CHARBOXPATH",
        "INTERVENE", "RETURN_WIDTH"
    };
    int i;
    gs_text_enum_t *pte;
    int code;
    gx_device_epilog *epilog = (gx_device_epilog *)dev;
    epilog_write(epilog, "");
    
    dmputs(dev->memory,"text_begin(");
    for (i = 0; i < countof(tags); ++i)
        if (text->operation & (1 << i)) {
            if (tags[i])
                dmprintf1(dev->memory,"%s ", tags[i]);
            else
                dmprintf1(dev->memory,"%d? ", i);
        }
    dmprintf1(dev->memory,"font=%s\n           text=(", font->font_name.chars);
    if (text->operation & TEXT_FROM_SINGLE_CHAR)
        dmprintf1(dev->memory,"0x%lx", (ulong)text->data.d_char);
    else if (text->operation & TEXT_FROM_SINGLE_GLYPH)
        dmprintf1(dev->memory,"0x%lx", (ulong)text->data.d_glyph);
    else
        for (i = 0; i < text->size; ++i) {
            if (text->operation & TEXT_FROM_STRING)
                dmputc(dev->memory,text->data.bytes[i]);
            else
                dmprintf1(dev->memory,"0x%lx ",
                          (text->operation & TEXT_FROM_GLYPHS ?
                           (ulong)text->data.glyphs[i] :
                           (ulong)text->data.chars[i]));
        }
    dmprintf1(dev->memory,")\n           size=%u", text->size);
    if (text->operation & TEXT_ADD_TO_ALL_WIDTHS)
        dmprintf2(dev->memory,", delta_all=(%g,%g)", text->delta_all.x, text->delta_all.y);
    if (text->operation & TEXT_ADD_TO_SPACE_WIDTH) {
        dmprintf3(dev->memory,", space=0x%lx, delta_space=(%g,%g)",
                  (text->operation & TEXT_FROM_GLYPHS ?
                   (ulong)text->space.s_glyph : (ulong)text->space.s_char),
                  text->delta_space.x, text->delta_space.y);
    }
    if (text->operation & TEXT_REPLACE_WIDTHS) {
        dmputs(dev->memory,"\n           widths=");
        for (i = 0; i < text->widths_size; ++i) {
            if (text->x_widths)
                dmprintf1(dev->memory,"(%g,", text->x_widths[i]);
            else
                dmputs(dev->memory,"(,");
            if (text->y_widths)
                dmprintf1(dev->memory,"%g)",
                          (text->y_widths == text->x_widths ?
                           text->y_widths[++i] : text->y_widths[i]));
            else
                dmputs(dev->memory,")");
        }
    }
    if (text->operation & TEXT_DO_DRAW)
        epilog_drawing_color(epilog,", ", pdcolor);
    /*
     * We can't do it if CHAR*PATH or INTERVENE, or if (RETURN_WIDTH and not
     * REPLACE_WIDTHS and we can't get the widths from the font).
     */
    if (text->operation &
        (TEXT_DO_FALSE_CHARPATH | TEXT_DO_TRUE_CHARPATH |
         TEXT_DO_FALSE_CHARBOXPATH | TEXT_DO_TRUE_CHARBOXPATH |
         TEXT_INTERVENE)
        )
        goto dflt;
    rc_alloc_struct_1(pte, gs_text_enum_t, &st_gs_text_enum, memory,
                      goto dflt, "epilog_text_begin");
    code = gs_text_enum_init(pte, &epilog_text_procs, dev, pis, text, font,
                             path, pdcolor, pcpath, memory);
    if (code < 0)
        goto dfree;
    if ((text->operation & (TEXT_DO_CHARWIDTH | TEXT_RETURN_WIDTH)) &&
        !(text->operation & TEXT_REPLACE_WIDTHS)
        ) {
        /*
         * Get the widths from the font.  This code is mostly copied from
         * the pdfwrite driver, and should probably be shared with it.
         * ****** WRONG IF Type 0 FONT ******
         */
        int i;
        gs_point w;
        double scale = (font->FontType == ft_TrueType ? 0.001 : 1.0);
        gs_fixed_point origin;
        gs_point dpt;
        int num_spaces = 0;
        
        if (!(text->operation & TEXT_FROM_STRING))
            goto dfree;		/* can't handle yet */
        if (gx_path_current_point(path, &origin) < 0)
            goto dfree;
        w.x = 0, w.y = 0;
        for (i = 0; i < text->size; ++i) {
            gs_char ch = text->data.bytes[i];
            int wmode = font->WMode;
            gs_glyph glyph =
            ((gs_font_base *)font)->procs.encode_char(font, ch,
                                                      GLYPH_SPACE_NAME);
            gs_glyph_info_t info;
            
            if (glyph != gs_no_glyph &&
                (code = font->procs.glyph_info(font, glyph, NULL,
                                               GLYPH_INFO_WIDTH0 << wmode,
                                               &info)) >= 0
                ) {
                w.x += info.width[wmode].x;
                w.y += info.width[wmode].y;
            } else
                goto dfree;
            if (ch == text->space.s_char)
                ++num_spaces;
        }
        gs_distance_transform(w.x * scale, w.y * scale,
                              &font->FontMatrix, &dpt);
        if (text->operation & TEXT_ADD_TO_ALL_WIDTHS) {
            int num_chars = text->size;
            
            dpt.x += text->delta_all.x * num_chars;
            dpt.y += text->delta_all.y * num_chars;
        }
        if (text->operation & TEXT_ADD_TO_SPACE_WIDTH) {
            dpt.x += text->delta_space.x * num_spaces;
            dpt.y += text->delta_space.y * num_spaces;
        }
        pte->returned.total_width = dpt;
        gs_distance_transform(dpt.x, dpt.y, &ctm_only(pis), &dpt);
        code = gx_path_add_point(path,
                                 origin.x + float2fixed(dpt.x),
                                 origin.y + float2fixed(dpt.y));
        if (code < 0)
            goto dfree;
    }
    dmputs(dev->memory,")\n");
    *ppenum = pte;
    return 0;
dfree:
    gs_free_object(memory, pte, "epilog_text_begin");
dflt:
    dmputs(dev->memory,") DEFAULTED\n");
    gx_fill_params p;
    p.rule = -1;
    p.flatness = 1;
    epilog_fill_path(dev, pis, path, &p, pdcolor, pcpath);
    return gx_default_text_begin(dev, pis, text, font, path, pdcolor,
                                 pcpath, memory, ppenum);
}


// Helper techniques

void tabMaker(gx_device_epilog * epilog)
{
    int i;
    static unsigned char lastTab = 0;
    if (lastTab == epilog->tabLevel) {
        return;
    }
    memset(epilog->tabs, 0, 256);
    for (i = 0; i < epilog->tabLevel; i++) {
        strcat(epilog->tabs, "\t");
    }
    lastTab = epilog->tabLevel;
}

void strokeString(gx_device_epilog *epilog,char* str,gx_path_type_t type)
{
    char tempBuf[EPILOG_LINESIZE];
    if ((epilog->strokecolor != gx_no_color_index) && (type & gx_path_type_stroke)) {
        sprintf(str, " stroke='#%06lx'", epilog->strokecolor & 0xffffffL);
    } else {
        sprintf(str, " stroke='none'");
    }
    if (type & gx_path_type_stroke) {
        sprintf(tempBuf, " stroke-width='%lf'", epilog->linewidth);
        strcat(str, tempBuf);
    }
    if ((epilog->fillcolor != gx_no_color_index) && (type & gx_path_type_fill)) {
        sprintf(tempBuf, " fill='#%06lx'", epilog->fillcolor & 0xffffffL);
    } else {
        sprintf(tempBuf, " fill='none'");
    }
    strcat(str, tempBuf);
    if (type & gx_path_type_even_odd) {
        strcat(str, " fillRule='evenOdd'");
    } else {
        strcat(str, " fillRule='winding'");
    }
    
}

void increaseIndent(gx_device_epilog * epilog)
{
    if(epilog->tabLevel < UCHAR_MAX)
        epilog->tabLevel++;
    tabMaker(epilog);
}

void decreaseIndent(gx_device_epilog * epilog)
{
    if(epilog->tabLevel > 0)
        epilog->tabLevel--;
    tabMaker(epilog);
}

void epilog_write_tabbed(gx_device_epilog * epilog,const char* str)
{
    epilog_write(epilog, epilog->tabs);
    epilog_write(epilog, str);
}

png_byte** makePNG(int height,int width,int depth)
{
    png_byte** arr;
    int i,j;
    arr = (png_byte**) malloc(height*sizeof(png_byte*));
    if (arr == 0) {
        return 0;
    }
    for (i = 0; i < height; i++) {
        arr[i] = (png_byte *) malloc(width*sizeof(png_byte)*depth);
        if (arr[i] == 0) {
            //Malloc failed free all memory we have alloc'ed
            for(j = 0;j < i; j++)
            {
                free(arr[i]);
            }
            free(arr);
            return 0;
        }
        //Set the memory to be "black 100% alpha"
        //We made this up
        memset(arr[i], 0x00, width*sizeof(png_byte)*depth);
    }
    return arr;
}

void freePNG(png_byte** arr,int height,int width)
{
    int i;
    if (arr == 0) {
        return;
    }
    for (i = 0; i < height; i++) {
        free(arr[i]);
    }
    free(arr);
    return;
}

void user_write_data(png_structp png_ptr, png_bytep data, png_size_t length)
{
    // Save the PNG data into an array for later encoding
    png_bytep newData = malloc(length+rawPNGLength);
    if(rawPNG){
        // Copy old data into new array
        memcpy(newData, rawPNG, rawPNGLength);
        // Cleanup old data
        free(rawPNG);
    }
    // Append new data
    memcpy(&newData[rawPNGLength], data, length);
    // Update total length
    rawPNGLength += length;
    // Update png data pointer
    rawPNG = newData;
}

void user_flush_data(png_structp png_ptr)
{
}
